## Priorities

- Get as far away from the danger as quickly as possible
- Keep the total weight of your gear low, aim for 10% of your body weight or less depending on your own fitness level.

## Essentials
Essentials will be based on situation, and environment. This is a work in progress

- cash. Credit card machines may be down
- meds
- Cell phone cable
- Battery pack
- [IFAK](#medical-and-hygiene)
- [clean socks](#weatherclothing)
- DEET, insects carry diseases
- good shoes for walking/hiking
- map of wherever you are, and compass
- multi-tool or pocket knife
- flashlight 
- whistle
- A hoodie, button shirt, or some long sleeved clean shirt to blend in, depends on the [weather](#weatherclothing)
- water and way to carry it. Purification may be important.
- food
- moleskine for blisters
- emergency space blanket
- poncho or tarp
- chapstick
- [firemaking](#fire-making)
- [self defense](#self-defense)
- hat


## Weather/clothing
- wool beanie
- gloves
- extra socks in a ziplock
- fleece, jacket, pullover
- sleeping bag, down quilt, ultra light tent, or tarp
- sleeping mat
- Buddy heater with propane
- Jet Boil stove with propane


## Medical and hygiene
- North American Rescue IFAK
- baby wipes
- chapstick
- baby powder, Monkey Butt, or Boudreaux's Butt Paste for chafing


## Self defense
- bear spray
- fire extinguisher
- dagger
- clinch pick
- CCW

## Fire Making
- lighter
- fat wood
- tinder
- matches
- large ferro rod (large helps for compromised motor control)

## Comms 
- Software defined radio
- HackerRF Portapack
- pirate box
- GoTenna Mesh
- Ham radios

## nice to have
- silcock key
- water purification
- knife sharpener
- pen, or pencil & paper
- walking sticks
- binoculars
- small folding saw (Silky)


## References

- [comms](https://www.offgridweb.com/gear/bag-loadout-urban-communications-kit/)
	- [Pirate Box tutorial](https://www.instructables.com/Raspberry-Pi-PirateBox/)
