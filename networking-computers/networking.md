## Resources

- [Cartoon guide to DNS over HTTPS](https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/)
- [How HTTPS works: a cartoon guide](https://howhttps.works/episodes/)
- [How DNS Works. A fun web comic tutorial](https://howdns.works/)
- [Illustrated guide to OAuth and OpenID Connect](https://developer.okta.com/blog/2019/10/21/illustrated-guide-to-oauth-and-oidc)

