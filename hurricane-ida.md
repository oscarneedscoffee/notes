## Latest on Ida

- [9/1/2020 Where to get gas WDSU](https://wdsu.com/article/list-where-to-get-gas/37446676)
- [8/31/2020 St. Tammany Parish Gov't open businesses](http://www.stpgov.org/open)
- [8/31/2021 DOTD I-10 Eastbound open](https://www.klfy.com/louisiana/dotd-i-10-eastbound-open-across-louisiana-still-some-blockages-on-westbound/)
- [8/30/2021 FEMA not paying for hotels](https://www.klfy.com/louisiana/fema-has-not-proposed-paying-for-hotels-in-louisiana-due-to-hurricane-ida/)
- [8/30/2021 Rouses open locations on Facebook](https://www.facebook.com/75466808547/posts/10159313882618548/?d=n)

## Lessons learned

- bring tools in a small bag
- bring small yoga mat, exercise is important after long drives
- It's easy to eat junk food: convenience, stress eating
