# Responsive Design

Here is a my collection of notes about responsive design. Since it's not my specialty, my notes are sparse and contained in this single file. 

---

<!-- TOC -->

- [Responsive Design](#responsive-design)
    - [General](#general)
        - [Perspective](#perspective)
        - [Content Strategy](#content-strategy)
        - [HTML](#html)
    - [Dao of web design](#dao-of-web-design)
        - [FONTS:](#fonts)
        - [LAYOUTS:](#layouts)
        - [COLORS:](#colors)
    - [Responsive Web Design](#responsive-web-design)
        - [Chapter 1:  Our Responsive Web](#chapter-1--our-responsive-web)
    - [Chapter 2:  The Flexible Grid](#chapter-2--the-flexible-grid)
    - [Bibliography](#bibliography)

<!-- /TOC -->

## General

### Perspective
In this age of mobile friendly sites, content is paramount.  You need to get your content strategy right before design begins.  But you should not design strictly for mobile.  All data should be able to be viewed regardless of the device since there is a diminishing line between mobile and non-mobile

### Content Strategy
Use the inverted pyramid. Starting with the content before the design will help you create a website that meets your business goals as well as the users’ needs.  

To manage your website’s content when moving to a new site, start with a content audit to see what you have. Go through it and pare it down, using only what needs to be on the site. Unnecessary content just makes the site more difficult for both the users and the site owner. (Petersen, Ch 1)

### HTML
```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

Avoid using `user-scalable=1` which would disable user zooming, unless it’s necessary. Add roles to `<banner>` `<header>` and `<nav>` to comply with WAI-ARIA.  This allows older versions of IE to process CSS for HTML 5 tags.  It’ll read the new HTML 5 tags as a regular `<span>`

<http://www.w3.org/TR/wai-aria/usage#usage_intro>
```html
<header>
<nav>
<article>
<aside>
<footer>
```

---

## Dao of web design 
[A List Apart Article on dao of web design](http://alistapart.com/article/dao)

don’t rely on any aspect of style sheets to work in order for a page to be accessible. Absolute units, like pixels and points are to be avoided (if that comes as a surprise, read on), and color needs to be used carefully, and never relied on.  

### FONTS:
Don’t use points or pixels. This allows users to choose the font size which suits them. Specify a font size as a percentage as some parent element. 

>“If you don’t set a size for the text in the BODY, then the text of the BODY will be the size that the reader has chosen as their default size. Already we are aiding adaptability of our page, simply by doing nothing!”

### LAYOUTS:
Again, use percentages or relative values for adaptable pages.  Margins, text indentation and other layout aspects can also be specified in relation to the size of the text they contain, using the `<em>` unit for specifying margins, text indentation and other layout aspects.  

If you specify `p  {margin-left: 1.5em}` you are saying that the left margin of paragraphs should be 1.5 times the height of the font of that paragraph. So, when a user adjusts their font size to make a page more legible, the margin increases proportionally, and if they adjust it to make it smaller, the margin adapts again.

### COLORS:
Use style sheets rather than HTML <em>.  Avoid relying on color combinations alone to convey meaning.

---

## Responsive Web Design 
**by Ethan Marcotte**

### Chapter 1:  Our Responsive Web
In this age of mobile friendly sites, content is paramount.  You need to get your content strategy right before design begins.  But you should not design strictly for mobile.  All data should be able to be viewed regardless of the device since there is a diminishing line between mobile and non-mobile.  

Don’t use fixed widths (see Tao of Web Design).  

>“Rather than creating spaces that influence the behavior of people that pass through them, responsive designers are investigating ways for a piece of architecture and its inhabitants to mutually influence and inform each other.”

We should let go of the self-imposed constraints on design.  Instead of creating sites for different specific devices, we should treat them as “facets of the same experience”

Ingredients needed for a responsive layout
1. A flexible, grid-based layout,
2. Flexible images and media, and
3. Media queries, a module from the CSS3 specification.

see <http://responsivewebdesign.com/robot/> as an example.

## Chapter 2:  The Flexible Grid
Robot or not?  
**example**: Start with a baseline font size in the body, and from there you can use <em> to change text sizes from that relative baseline.

To get font-size values in terms of em take the target font size from the comp, and divide it by the font-size of its containing elements.

*target ÷ context = result* 
so if your h1’s target size is 24 px and your 100% font-size is 16px, then your result is  24/16 = 1.5 em    

---

## Bibliography

**Learning Responsive Web Design**
By: Peterson, Clarissa. 
O'Reilly & Associates 
2014

**Responsive Web Design (Brief Books for People Who Make Websites, No. 4)**
By: Marcotte, Ethan
A Book Apart
2011