- <https://www.c-sharpcorner.com/UploadFile/9582c9/basic-C-Sharp-programming-problems-part3/>
- <https://odetocode.com/blogs/scott/archive/2013/08/07/deliberate-practice-in-tests.aspx>
- <https://www.pluralsight.com/courses/csharp-fundamentals-dev>
- <https://social.msdn.microsoft.com/Forums/vstudio/en-US/dd287cf9-a663-449f-b3f9-fd4b526f3005/suggest-some-deliberate-practice-to-improve-c-experience?forum=csharpgeneral>
- <http://www.programmr.com/blogs/deliberate-practice-coding-katas>
- <https://www.codingblocks.net/podcast/78-deliberate-practice-for-programmers/>