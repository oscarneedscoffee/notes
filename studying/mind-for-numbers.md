# A Mind for Numbers
by Barbara Oakley

## General

Refer to Oakley's [Coursera course](coursera-how-to-learn/coursera-how-to-learn.md)

- For learning new things, it's a good idea to turn off your precision-focused thinking and use diffuse mode (19)
- To solve difficult problems, we need deep focus, but as long as we're focusing, we're blocking the diffuse mode (22).
- \* **Embrace befuddlement** (22). Learning is all about working out the confusion. "Articulating your questions is 80% of your game.

<!---------- Learning is creating --------------------------->
## Learning Is Creating (Ch 3)

- list of diffuse mode tools (35). Be careful of diffuse activities that may pull you into focused.
- To get out of the Einstellung Effect, look away from the problem.

### Avoid the Einstellung Effect (Getting Stuck)

- Figuring out a hard problem requires periods where you are not actively working out the problem (38). See brick and mortar example.

<!------------------- BREAKS ------------------------------------->
## Breaks (Ch 4)

How long of a break while in diffuse mode depends on you, but no more than a day.

<!----------------- MEMORY ---------------------------------->
## Memory

The two types of memory are **working memory** and **long term memory**.  Your working memory is composed of chunks of 4. Like a warehouse, your long term memory can hold more, but needs repetition spaced days apart to stick. This is called **spaced repetition**

<!------------------ SLEEP ----------------------------------->
## Sleep (Ch 3)

- You buildup toxins while awake. Sleep helps to flush them out. It's best to focus on a thing right before sleep, even better if you dream it.
- It's better to go to bed early and read the next day than to read until late.

<!---------------- Chunking & Avoiding Illusions of Competence ------------>
## Chunking and Avoiding Illusions of Competence (Ch 4)

Chunking is taking separate bits of info and putting them together.

### Steps to Chunking:

1. Focus on subject to chunk
2. Understand what you are chunking (the basic idea)
3. Put your chunked information into context so you know _how_ and _when_ to chunk

Chunking is top down and bottom up. Context is where they meet.

<img src="chunking.png" alt="The chunking process" width="400px" />

### Illusions of Competence and the Importance of Recall

- It is more effective to attempt to recall something \*than re-reading\*
- Practice recalling in different environments. E.g. Take a walk outside, that way you don't rely on visual cues alone for recall

Interleaving is practice with a mix of problems with different strategies. This contrasts with overlearning which may not be optimal.

Understanding is not enough. You should practice and repeat using interleaving, and spaced repetition.