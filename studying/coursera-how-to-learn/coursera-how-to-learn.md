# Coursera Course: Learning How to Learn

## About

Coursera course: Learning How to Learn with Barbara Oakley, and Terrance Sejnowski

## Brain dump

- Math and science can be hard to learn because of abstractness, and encryptedness. For example, there may be no physical representation
of addition so we abstract it out. We represent it with '+' symbol, but that can be "encrypted" to mean other things.
- One problem for students is in learning math and science is they try to jump in the water before they learn to swim.
e.g. They blindly start a homework before they read the textbook.
- Embrace beffudlement. Learning is all about working out the confusion.  
    - >Articulating your question is 80% of the battle

<!------------------------ WEEK 1 ----------------------------------------------->
## Week 1

### Modes of thinking

Focused and diffuse modes of thinking. It's not possible to think both ways at the same time. 
Focused mode can be thought of as a pinball machine with tightly spaced bumpers. Diffuse there are fewer bumpers and they
are wider apart.

- You may suffer from the **Einstellung Effect** where a have an idea for a solution, but this idea and perspective prevents you
from a better solution. This comes from staying in the focused mode.
- To solve different problems, we need deep focus, but as long as we're focusing we're blocking the diffuse mode
- For learning new things, it's a good idea to turn off your precision-focused thinking and use the diffuse mode (Oakley, 19)

### Procrastination

Procrastination can be superficial focused mode. You're not focused on what you actual should. If you find yourself
procrastinating, try focusing in 25 minute increments, then take a break, and go back *See Pomodoro Method* (Oakley, 24).

### Practice Makes Perfect

Practice strengthens the neural connections. The more abstract, the more important it is to practice. A little practice a day versus cramming helps diffuse thinking.

### Memory Long Term Memory and Working Memory

When you first store into long term memory, you need to repeat it to strengthen that connection. A technique to store LTM is __spaced repetition__.

### Sleep

Just begin awake causes toxic buildup, Sleep flushes it out.

### Interview with Terry Sejnowski

- Learn by doing
- To stay engaged, ask questions; this makes you to think actively
- Gets more done at night when he's left at home. He multitasks-- unavoidable. To him multitasking is context switching.
- To increase neurons-- active stimulation. In the absence of stimulation, **exercise** will increase neurons.
- Being with other creative people helps keeps him creative
- Test taking is a a skill. If you can't answer a question move on and come back.
- Success is passion and persistence; that is the most important quality for success.

### Summary of Week 1

**Focused Mode:** Good for familiar things.  
**Diffuse Mode:** The broad range view of diffuse mode is good for learning _new_ things.

---

<!----------------------------- WEEK 2 ------------------------------------------>
## Week 2

### Chunking and the Illusions of Competence in Learning

The "Octopus of Attention" in focused mode uses working memory to connect parts of brain to learn.

**Chunk**: A piece of information that is bound together by meaning or use.  

Gaining expertise involves uniting mental chunks. Focus, practice, and repetition helps build chunks.  Chunking helps you get the main idea without overthinking all the detail.

### How to Form a Chunk

Look at patterns, grasp mini-chunks, and weave them into a single chunk, then combine those into a _neural pattern_. Learn it bit by bit.

ex: _Listen to a song before playing it yourself_

### How to Chunk

1. Focus on the subject to chunk
2. Understand the basic concept of the subject.  
    - Aha! (understanding) doesn't mean you created a chunk. Only _doing_ it yourself helps you understand
3. Gain context so you can see how and when to use the chunk

### Illusions of Competence, Recall, Mini-testing, and the Value of Making Mistakes

Jeff Karpicke did research on _illusions of competence_ in learning.   
> e.g. _Re-reading text over and over rather than recalling is not learning._  

Recall is a way to test yourself to see if you really learned. If you make a mistake in recall, it's an opportunity to make repairs in your tiny self tests before you make a more higher stakes mistake later.  

Using _chunks_ to help you understand similar chunks is called **transfer**. Diffuse mode can help you move to other chunks to see the bigger picture.

Here is a diagram from [Oakley's book](../mind-for-numbers.md#steps-to-chunking:) that helps to illustrate chunking's relation to the context and the big picture

<img src="../chunking.png" width="400px">

### Seing the Bigger Picture

#### Ways to solve problems:

1. **Sequential thinking:** focused mode, deliberate step-by-step
2. **Holistic thinking:** Uses diffuse mode and is what you mostly use in creative thinking, and links several focused mode thoughts


---
<!----------- A MIND FOR NUMBERS by Barbara Oakley --------------------->
## Learning is Creating (Chapter 3, Oakley)

Start off with focused mode thinking, and then take a break and go into diffused mode. If you get stuck in the *Einstellung Effect*, get away from the problem and pick some diffused mode activites. See the list on p. 35 of [Oakley book](#resources).  

Be careful not to pick diffuse mode activites that may pull you into focused mode: e.g. social media, video gaming, figuring out another problem for a friend.

### Breaks

How long of a break to take for diffuse is up to you. It can be a few minutes, or hours, but no more than a day. This will help
us retain the information.

### Working Memory and Long Term Memory

Working memory is composed of only four chunks, so it's helpful to let some problems work themselves out in the background. But 
we have a finite amount of willpower to keep it churning, so it's helpful to take a break.

Long term memory is like a warehouse, but we're more likely to recall something if there are enough repetitions. These repetitions
should occur over a period of days not hours. This is called **SPACED REPETITION**  

### Sleep

You build up toxins when you're awake, but sleep helps us flush them out. If you're learning something difficult, it helps to read
about it right before bed, and your diffuse mode brain can work it out during sleep. It's even better if you dream about it.

It's far better to go to bed early when you're tired, and rise early to read the next day than to read until late, and wake later. 
You'll be able to retain more when you are rested.

More notes from "A Mind for Numbers" by Oakley [here:](../mind-for-numbers.md)

---

<!---------------- RESOURCES --------------------------------------------->
## Resources

- [Coursera link: Learning How to Learn](https://www.coursera.org/learn/learning-how-to-learn/)
- [*Mind for Numbers* (Barbara Oakley, 2014)](../mind-for-numbers.md)
- [Reddit discussion with notes in comments](https://www.reddit.com/r/GetMotivated/comments/5950tm/text_i_just_finished_the_online_coursera_course/)
