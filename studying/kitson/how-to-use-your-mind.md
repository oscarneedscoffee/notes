# How to Use Your Mind to Study: A Psychology of Study

by Harry Kitson
<!-- TOC -->autoauto- [How to Use Your Mind to Study: A Psychology of Study](#how-to-use-your-mind-to-study-a-psychology-of-study)auto    - [Formation of habits](#formation-of-habits)auto    - [Note Taking](#note-taking)auto    - [Reading Notes](#reading-notes)auto    - [Steps to take when reading a book (31 - 34)](#steps-to-take-when-reading-a-book-31---34)auto    - [Lab Notes](#lab-notes)auto    - [Brain Action During Study](#brain-action-during-study)auto        - [Properties of Nerve Cells Important in Study (43)](#properties-of-nerve-cells-important-in-study-43)autoauto<!-- /TOC -->
## Formation of habits

>**Exercise:**
>
> Find a habit you want to break. Describe the habit you want to change. Give concrete steops you'll do to form the new habit. Estimate how longto form the new habit and mark the date and see how accurately you estimated.

ex: Waking up late is a bad habit. Replace it with Waking up earlier, and use a habit tracker to track results.

## Note Taking

Lessons are often a series of disconnected parts in a student's mind like an unlinked chain, but should be a summary of preceding lessons and preparation of the next lessons connects the links.


## Reading Notes

Notes should be a summary. Simply copying the words in a book in single sentences spoils the continuity of thought and application. Transcription is expensive.

Isolated sentences mean little and fail to represent the thoughts of the author. What you can do is read a paragraph, close the book, and reproduce it in your own words.

Next compare your summary to what is in the paragraph to make sure you really got the point. This ensures you are following the thought and not just reading the words. **You're a thinker not a sponge**

>"Read not to contradict, nor to believe, but to weigh and consider." 
>
>from the _Reading Room, Harper Memorial Library, University of Chicago_


> "Don't read to swallow; read to choose, for 'tis but to see what one has use for" 
>                                                              
> _- Ibsen (p 33)_

**Before** you read ask yourself _"What am I looking for?"_ When you read what you need, close the book and summarize to see if the author furnished what you were looking for.  ` * ` _Read for a purpose!_



## Steps to take when reading a book (31 - 34)

1. Observe the title
1. Observe the author's name. If you're going to usethe book regularly, find his position in the field.
1. Glance over the preface. Make friends with the author; let him introduce himself to you; this he will do in the preface. Observe the publication date and recency of the material.
1. Glance over the table of contents; study it.
1. Use the index intelligently; it may save you time. Be scrupulous in making bibliographies.


## Lab Notes

Strive to be neat. Messy notes may bias the instructor against you.

> Lab notes suggestions
>
> - Write neatly
> - Write in ink
> - Use complete sentences
> - Get a notebook with a size and shape you're comfortable with.

Develop your form of note taking and stick with it. A less obvious effect of good note taking is helpful in making you a better thinker. **Correct note taking leads to correct thinking.**


## Brain Action During Study

When you're thinking, you make changes to your nervous system. How does it work?

### Properties of Nerve Cells Important in Study (43)

The nervous system uses the following properties:

* Impressibility
* Conductivity
* Modifiability

We can view education as an aggregation of processes using those properties.
