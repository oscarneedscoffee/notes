# Maximum Energy for Life
by Mackie Shilstone

## 7 Secrets to Put You at the Top of Your Form

1. Reduce your health age to increase your performance  
    - Poor health dulls your perfomance edge

2. Reduce your fatigue threshold
    - See Lomas Brown Example

3. Manage your performance to go the distance
    - Manage your energy like an athlete (e.g. boxer, wrestler)

        >"The competitive challenge in life as in sports is to maintain
        >your own energy levels while pushing your opponent into a
        >state of overuse and overreaching"

    - Always perform with integrity
    - Never overreach yourself. Don't allow others to maneuver you into overreaching.

4. Control your emotions for maximum performance
    - Emotional equilibrium is key to high performance

5. Keep your work life and your personal life balanced
    - if you can focus, communicate and plane ahead, you can achieve balance between maximum performance in career & home.

6. Learn to anticipate life's next moves
    - Anticipating moves ensures your energy goes towards the actions most needed, and not wasting precious energy going back & forth.
    - We need to patiently study and understand those we compete against in life and work place
        - Tips:  
            1.  Head off younger/less experienced workers after your job.
            2. Read signs of industry change and be ready for a major downturn or shift  
            3.  See opportunities down the road, and position yourself to grasp it when it presents itself to you  
            4. Never become complacent in your career or anywhere in life. Educate yourself about new things in your field.  
            5. Seek out willing mentors.

7. Perform well to your last breath
    >"If how you lived is determined by how you die, what performance
    >strategies must you develop to work and live with gusto?"
    >"I want to die young as late as possible" - Dr. Christiaan Barnard
    - Lifestyle choices we make daily determine our performance.

---

## Ch 3 Nine Strategies to Achieve Your Goals

1. Don't allow others to say you cannot achieve your goals
2. Evaluate where you are at this point
3. Know where you want to go in the long run
4. Use dreams & visualizations to see yourself there
5. Make incrementl steps towards your goal.
6. Anticipate the competition and opponent's strategy
7. Develop a strategy for distracting the competition long enough to give you an opening advantage
8. Develop the instinct to make the lateral move when the punch is coming so you don't get hurt.
9. Learn to relax.