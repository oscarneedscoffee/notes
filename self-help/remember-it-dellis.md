# Remember It

by Nelson Dellis  

## See Link Go! 

page 29.  
**See** the picture in your mind. **Link** the thing in your mind to a known location, and **Go** let your memory run wild!

The different ways to see and link are:

1. **Association**: Link the memory with something that is known.
2. **Link Method**: Good for a quick list in order. You only need to remember the first item.
3. **Peg Method**: Lets you remember the first item.
4. **Journey Method**: Uses location and is very effective. Ancient Greeks called it the *Memory Palace*

### SOGAMA

A way to make links stick is with **SO GA MA** (sorta like "So, Gramma").

- **SO Sensory Overload**: Use all your senses
- **GA Grotesque Absurdity**: Make your image wild, the crazier the better.
- **MA Movable Attribute**: Give it movement

## Paying Attention Sec. 1 Ch 3

You cannot remember if you don't pay attention. Put down the phone or laptop. One way of remember a new piece of info is with **anchoring**

**Anchoring** is associating new information with other already grounded information. (p. 49)