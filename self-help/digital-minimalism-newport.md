# Digital Minimalism

by Cal Newport

## 3 Principles of DM

1. Clutter is costly
2. Optimization is important
3. Intentionality is satisfying

Newport advocates for a technological *philosophy of use*. This is a complete from the ground up review of what a piece of tech adds to our lives. (part 1)

## An argument for pinciple 1: Thoreau's New Economies

His pragmatic side is often overlooked in Walden. He campares the cost of his cabin and the one day of work required to sustain it versus the overhead of a large farm with better furnishings. Are those things worth the things in life you're missing?

>"The cost of thing is the amount of what I will call life which is required to be exchanged for it, immediately or in the long run" (See farmer example p39)

Newport: "What these farmers are actually getting from this life they sacrifice is slightly nicer stuff.

Thoreau's new economies demand you balance profit against the costs measured in terms of your life.  

The costs of digital distractions compound. This can leave you crushed and smothered under the demands of your time and attention like Thoreau's farmers.  

All you get from your sacrifice are slightly nicer trinkets. \*This is why clutter is so dangerous.

So what made Thoreau so insightful? I was nothing that hadn't aoready been said-- it was his obsession with calculations. That helped us move from vague subjectiveness that there are tradeoffs in clutter and forces us to confront it more precisely.  

>Treat the minutes of your lives as a concrete and valuable substance

## An Argument for Principle 2: The Return Curve