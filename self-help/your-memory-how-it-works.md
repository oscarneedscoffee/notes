#  Your Memory: How it Works and How to Improve It

Author:
Kenneth Higbee, Professor of Psychology

<!-- TOC -->

- [Your Memory: How it Works and How to Improve It](#your-memory-how-it-works-and-how-to-improve-it)
    - [CHAPTER 1  TEN MYTHS ABOUT MEMORY](#chapter-1--ten-myths-about-memory)
        - [SUMMARY](#summary)
    - [CHAPTER 2  MEET YOUR MEMORY: WHAT IS IT?](#chapter-2--meet-your-memory-what-is-it)
        - [Remembering is viewed as three stages](#remembering-is-viewed-as-three-stages)
        - [WHAT IS SHORT TERM MEMORY?](#what-is-short-term-memory)
        - [What is Chunking?](#what-is-chunking)
        - [WHAT IS LONG TERM MEMORY?](#what-is-long-term-memory)
            - [The three types of LTM](#the-three-types-of-ltm)
        - [The Components of Memory and Learning](#the-components-of-memory-and-learning)
        - [The Measures of Memory](#the-measures-of-memory)

<!-- /TOC -->

## CHAPTER 1  TEN MYTHS ABOUT MEMORY 

1. Memory is a thing
1. There is a secret to a good memory
1. There is an easy way to memorize
1. Some people are stuck with bad memories
1. Some people are blessed with photographic memories
1. Some people are blessed with photographic memories
1. Memory, like a muscle, benefits from exercise
1. A trained memory never forgets
1. Remembering too much can clutter your mind
1. People only use 10 percent of their mental potential

### SUMMARY

There are myths associated with memory.  Memory is not a thing, but a process and requires effort and organization.

<!-- ***********  Chapter 2  ************** -->

## CHAPTER 2  MEET YOUR MEMORY: WHAT IS IT?

### Remembering is viewed as three stages

1. Acquisition or encoding
1. Storage
1. Retrieval

You can use this mnemonic — The Three R’s of remembering:  *Recording, Retaining, and Retrieval*

Most problems in remembering come at the retrieval stage and not storage.  More can be stored in memory than retrieved.  We can’t do much to improve retrieval directly, but retrieval is a function of how info is recorded and retained. 

It is useful to distinguish between memory that is accessible from what is available.  Higsbee example is the keys in the well.  Your car keys may be in a well so you know it’s available, but it’s not accessible because they cannot be retrieved.   

Most psychologists agree on two types of memory: short term and long term

### WHAT IS SHORT TERM MEMORY?
Short term memory is what used to be called “Attention Span” and it has a rapid forgetting rate of 30 seconds or much less.  The usual way to combat forgetting is by rehearsing.  Rehearsing helps to keep the information in short term memory and also gives you more time to put it in long term memory.  A way to increase short term memory is by chunking

### What is Chunking?
Chunking is simply grouping discrete pieces of information into larger chunks.  Higbee give the telephone number example.  It’s easier to remember 504-865-8874 than to remember 5048658874.   Chunking does take time, so if items come too fast, then it’s not as effective.

     Short term memory has such a limited capacity, you may wonder what it’s good for?  STM is like a scratch pad for information.  We may not want to retain all this information.  If we memorized every thing we saw, our minds would be too cluttered.  STM helps us keep track of current information easier.

### WHAT IS LONG TERM MEMORY?

#### The three types of LTM

1. Procedural:  skills
1. Semantic:  Remembering factual information
1. Episodic

### The Components of Memory and Learning
Meaningfulness, Organization, Associativity, Visualization, Attention  (MOAVA  hint: say “memory move-a”)

Meaningfulness
Make it meaningful with familiarity, rhymes, and patterns

### The Measures of Memory
Recall, Recognition, and Relearning
