# Grit

by Angela Duckworth  

## Deliberate Practice

- Ben Franklin example: He would rewrite his favorite essays by other authors until his writing was better
- 13 year old spelling champ Kerry Cloe used quizzing to find weaknesses. Quizzes were emphasized in *Learning How to Learn* by Barbara Oakley
- Deliberate practice is exhausting. Most get tired after an hour and can do no more than 5 hours a day - psychologist, Anders Ericsson
- Mihaly Czikszentmihaly `"cheeks-sent-me-high"` and flow
- Mihaly and Ericsson have different perspectives but Duckworth argues "deliberate practice is a behavior & flow is an experience"
- Deliberate practice is problem solving mode and probably not pleasurable. You are getting feedback, often, negativeand you're trying to improve. Flow is an experience that is "intrinsically pleasurable". Duckworth states 
  >deliberate practice is for preparation, flow for performance

### Deliberate pracitce requirements:

- Clearly defined stretch goals 
- Full concentration and effort
- Immediate and informative feedback
- Repetition with reflection & refinement
  > "Even supermotivated people who're working to exhaustion may not be doing deliberate practice"

To get the most of out deliberate practice:  

1. Focus on your weaknesses
2. Make it a habit

## Ch 8 Purpose

3 Stages:
Start with self interest > Self-disciplined practice > Integrate work with other-centered purpose