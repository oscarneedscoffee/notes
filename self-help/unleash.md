# Unleash the Warrior Within by Richard Machowicz

>“You don't have to like it, you just have to do it” (8)

>“You have to create little moments of victory for yourself no matter how small or how short they last” (10)

>“Being a warrior is not about fighting. It's about being so prepared to face a challenge and believing so strongly in the cause you are fighting for that you refuse to quit. “(15)

## THE THREE DYNAMIC ELEMENTS OF COMBAT CHAPTER 1

### The Elements

1. Target
2. Weapons
3. Movement

### Target dictates weapons. Weapons dictate movement (20)

Get everything out of the way of the target. Then you will clearly see what weapons you need and what movement is required (22). Without a focused mind, results are happenstance, unreliable. Thought precedes every action, so in order to focus the mind, you need a way to streamline your thinking.

### Be Target Driven

You can't afford to abandon a target in the process of knocking it down, because when you do that you lose the power of focus and concentration (24).

>"....movement, how you get your weapons to your target, is ultimately flexible. Keep in mind balance" (25)


### Carver Matrix

Assess targets with the CARVER matrix (20)

1. **Criticality** How vital to the overall mission?
2. **Accessibility** How easily can I get to this target?
3. **Recognizability** How easy can I find the target?
4. **Vulnerability** What degree of force needed to destroy the target?
5. **Effect on the Overall Mission** To what degree will the destruction of this target affect my enemy?
6. **Return on Effort (Recuperability)** Can the enemy recover from the destruction of this target?


### Other notes on the three dynamic elements

* Concentrate on destroying at least one of our 1st strike targets before we proceed to anything else.
* Stop looking to others to give you permission to go after the target you want to knock down. (33)
* Stop waiting for the ideal situation. There is no such thing
* Once you have decided on a particular target, you must make it your mission to achieve it. This mission must become the central focus of your time and energy


### Gut check to take out the target (34)

* [ ] Are you willing to make a choice?
* [ ] Do you have the courage to start?
* [ ] Can you make the commitment to finish?

>Once you have answered "yes" to gut check, you are ready to acquire the target.

Now that you understand your target dictates the weapons, and the weapons dictate movement you can appreciate one more thing: _It's not until you make a target a matter of life and death that you end up living._

---

## CRUSH THE ENEMY CALLED FEAR CHAPTER 2 (36)

### Three Elements of a Successful Attack (45)

1. Surprise A surprised enemy will have greater stress
2. Speed Affords the enemy little time to evaluate
3. Violence of Action All about the intensity and commitment of your action. It leaves no possibility for recovery.

Surprises happen in life, but that is not the same as saying life is full of surprises.

If you live it that way, if you live life without planning the most basic actions, you life purely as a reaction If you say to yourself, I'm just going to react to whatever happens, then you're going to face a lot more fear (46). _This is similar to how Musashi trains in “...5 Rings”._

### Improved Outcome Formula (46)

The I.O.F is a way to improve the outcome of any situation

_Three elements of the Improved Outcome Formula_

1. Constant I am the only constant that will be present in every situation I face.
2. Improvement Anything I do to better myself, anything. Even a simple attitude adjustment can greatly help
3. Situation You are not the situation. Separate yourself from the situation

Often you put yourself in situations by volunteering or through neglecting responsibility (47).

Work harder on controlling yourself than you do trying to control your situation (48)._ See his Hell Week story on the same page_


### Focused mind (50)

The way to destroy an attacker is to destroy the attacker's mind. You can damage a man's body, but if you don't damage him in a way that undercuts his very thinking, he can come back. But if you destroy his mind, there's no way his body can come back. The mind is the fire. Extinguish that, you destroy the blaze.

### Pain & chaos (51). 

You can focus through pain. In chaos, you can't. The way to conquer fear is to move into and through it. (51)

### Bias (53)

>“As adults, we have so many references based in life experience. We tend to draw conclusions from those references, and in the process we often transfer fears..”

### Culture of Fear (55-56)

Mack categorizes fear as controlled and uncontrolled . Fear is like any problem and if you want a solution, first recognize the problem.

The culture of fear keeps us in jobs we hate, relationships that are hurtful to our self-esteem, houses we don't like and in neighborhoods we have outgrown. (56)

Only two things can get in the way of knocking down a target: lack of knowledge and fear (59)

Fear only causes me to react. Fear only causes me to wait. Fear moves me away from effective action. When you find yourself acting like a jerk, stop for a second and just ask yourself, ‘What am I afraid of here?’

Machowicz believes there are two root emotions to everything a person experiences: You are either moving toward fear or love. (61).

All negative emotions are attached to some fear without exception (62).

“Aggressively live life” (63)

### The Fear Crusher (64-66)

You can deal with your fears with this method. Here are the two parts.

#### Section I: Pride Drill

1. Write down a moment in life when you felt you really accomplished something, large or small.
2. Write down all the things that you value about the idea of hitting your current targets. Turn all those things into one powerful motivating sentence.
3. Write down three things that happen to you when you are nervous or afraid
4. Create an action trigger. This is a command that propels your mind and body into action. Examples: “Go for it!” “Showtime!”

### Section II: Moving Through Fear

1. Recognize the telltale signs of fear. Physical behavior, negative words you say to yourself.
2. Recall the sense of accomplishment you wrote down in your pride drill. Feel it as if it were happening again.
3. Repeat your powerful motivating sentence three times.
4. Calmly and simply state what it is you are going to accomplish.
5. Take a deep breath and let it all out.
6. Pull your action trigger: “Showtime!” “Go for it!”
7. Go!!! He who dares, wins.

---

## CREATE AN ACTION MINDSET CH 3 (68)

> “An action mindset is the state of mind you need to have in order to handle any situation” (68)

### Reactive vs. Active Mind

A reactive mind only reacts through fear. It’s unable to do what it takes to handle the situation. The active mind is thinking and is taking out the target.

### Internal dialogue affects performance: (70)

You can talk yourself into failing. Negative speech can become a self-fulfilling prophecy.

### Verbal Influence Conditioning

Creating an action mindset begins with V.I.C. VIC will let you take an aggressive and systematic approach to your “self-talk” that will get things done (72).

* You have to become distinctly aware of the words you choose to say to yourself, and what VIC you’re creating
* The fact that we can change our feelings means we have power over them

### How to find the Verbal Influence Conditioning that works against you and turn it positive:

1. Mission: Keep a journal for 3 days straight so you can see your thoughts
2. Don’t give yourself an out. If you do, you’ll use it when things get tough.

Highlight the negative & positive words in two different colors. This will help show you what you’re thinking and how you’re communicating it. Every month do this with yourself.

### Bukido’s Code of the Warrior Spirit (79-80)

1. I will be responsible for my life and my actions
2. I will concentrate all the energy of my body and mind on one specific target at a time
3. I will develop the ability to remain calm and composed, for even in the worst situations I will find opportunity
4. I will spend my time wisely, for it is too precious a commodity to waste.
5. I will continually challenge myself to learn by exposing myself to others with skills greater than my own.
6. I will develop patience in all things, for it is the essential quality of a powerful mind
7. I will make my capabilities exceed my limitations.

**Action Mind-Set**: The process of recognizing and moving beyond your fear to completely focus on the knowns. The process begins by understanding the words we use that influence our thoughts and condition our actions.

**Verbal Request Command:** A command you use on yourself or another that’s structured in a specific way to elicit a known result.

An example of VCR is Nike’s “Just Do It” The simpler the command, the easier it is to remember and the easier it is to recall under extreme stress (81). A professional doesn’t quit until every task is complete (83). Every resource you have and commitment is driven by a **target**. That target is give a label and that label becomes preloaded “_Action Trigger_” (84)

For a verbal command request to work, you need to be very specific about what you want it to accomplish. It has to bring back that same intensity every single time you use it (85).

### Conflict

The reason why a person would attack someone else? Because they can. (86)

#### Prelude to a Conflict (88-90)

1. The Pitch
2. The Warning
3. The Standoff
4. Crossing the line

One person is going to be the cause, the other is going to be the effect. The list above will work to control the destination of the conflict only if you don’t get caught up in the dialogue of the moment.

When you are more concerned about coming across as mean, or witty, or superior, you become ensnared in the event, and you are not able to see it clearly (90).

You may not be able to control the situation (or person), but you can control yourself. There are times when you want to use VCR to escalate the emotional level of the situation. For instance if you do sales for a living.

### Moving Through the Action Mind-Set (93)

1. **Pick** your target
2. **Recognize** your fear
3. **Understand** the VIC that works against your desire for a new and better position
4. **Deploy** a Verbal Command Request
5. **Move through** and beyond whatever fear comes up
6. **Completely focus** on your known action

### 7 Keys to Minimizing Reaction Time (95)

1. Limit your response time
2. Simpler techniques are faster techniques
3. Practice your techniques
4. Position yourself so that your actions are smooth, natural, and efficient
5. Remember, you can react later with a fast (simple) technique than earlier with a slow (complicated) technique
6. Anticipate correctly and gain a little, anticipate incorrectly and lose a lot.
7. All reaction time is shortened with advance information. 

---

## THE FOUR CRITICAL KEYS TO CONQUERING ANYTHING ACTE (CHAPTER 4)

### (ACTE) The four Keys (98)

1. Assess the situation
2. Create an action plan
3. Take action
4. Evaluate progress

In warfare, there’s no room for ego. Warfare is all about strategic thinking. Strategic thinking is more than just planning. It’s about gaining the greatest advantage and making the fewest mistakes (100).

### When in doubt ACTE (101)

**Assess** the situation: Solutions naturally evolve when you know what you are dealing with.

**Create** a simple plan: A simple plan is easy to remember and easy to perform and leads to a faster result.

**Take** action: The more direct your route in taking action, the quicker your result.

**Evaluate** your progress: What happened? have my plan and my actions created my desired results? If not, start over. Assess the situation again, create a simple plan, take action. In evaluating anything, the “100 Percent Rule” should be your measuring stick: If it works 100 Percent of the time, it stays. If it doesn’t, fix it. If you can’t fix it, get rid of it.

If you live life passively, you are forcing yourself to live reactively. One choice, no matter how small can save you from “_analysis paralysis_” where you spend more time thinking about doing something than actually doing it (101). Sometimes you don’t notice things that are right in front of you because you’re too busy looking ahead. The closest threat is the one you have to pick up first, not last.

You can use _systematic observation_ (104). Instead of looking to knock down vague far away targets you should define a target that is right next to you. Scan from right to left what is closest in a sweeping motion.

To find your _closest targets_ yo need to assess four major factors (105).

1. What do I have?
2. What do I need?
3. What are my strengths?
4. What are my weaknesses?

No matter what your long term goal is, short-term targets are in front of it. That means as you use the scanning process.

Machowicz rarely goes beyond his secondary list of targets because a lot can change after knocking down his primaries. He wants to stay flexible