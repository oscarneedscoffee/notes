# D-Day: June 6, 1944: The Climatic Battle of WWII                     

**by Stephen Ambrose**

## Numbers 

-	U.S. , British & Canadian Divisions (Infantry) were 15 – 20,000 strong on D-Day
-	Airborne ½ that
-	German divisions < 10,000
-	Participants:  175,000 British, U.S., Canadian, Free French, Norwegian, Polish,
Etc.  50,000 vehicles, 5,333ships, 11,000 planes. 
-	Overlord started at 0016 hrs June 6, 1944 British double daylight savings time


## GERMAN WEAPONS
-	Leuchtpistole: German flare gun
-	Maschinengewehr (MG-34)    Gewehrs: rifles   Karabiners (carbines)
-	Flakvierling-38’s (20mm 4-barreled antiaircraft gun)   (p.22)

## ALLIED WEAPONS  ( * see p. 585)
-	C-47: U.S. military version of the DC-10
-	Higgins landing craft LCVP (Landing Craft  Vehicle, Personnel)
-	LST (landing ship, Tank) & LCT (Landing Craft Tank)


## IMPORTANT PLAYERS ON D-DAY
- Erwin Rommel (b.1891 Gmund, Swabia)  entered the Royal Officer Cadet School in Danzig 1910.
- Dwight Eisenhower (b.1890 Abilene, Kansas) entered West Point 1911
- Field Marshal Gerd von Rundstedt: German commander in the west.  Rommel’s boss

## IMPORTANT LOCATIONS
- See maps on pps. 78 and 87
- Beaches of Normandy S-N.  Utah, Omaha, Gold, Juno, Sword

## IMPORTANT OPERATIONS
- Fortitude. An Allied diversionary plan to draw forces away from Seine.


