<!-------------------------- CH 6 ---------------------------------->
## Chapter 6: I’ve got the Right Horse Here (149)

- CNBC is selling an investment dream not borne of stats
- Most people will not beat the stock market indexes. Period
- Bloomberg doesn’t get MSNBC’s ratings but is more respectable.

---
<!-------------------------- CH 7 ---------------------------------->
## Ch 7 An Empire of Her Own

- Prudential & Wachovia have been harping tht women need help from financial advisers for years (Ch. 7, 151)
- Women control $16.2 Trillion in assest but believe they know less than men (152)
- Companies focus on cliches of women so women stay as customers
- Women make less than men, not because they are bad with money, but because they make less and live longer (153)
- A financial advisor can't make up for a woman's lost wages. Olen hints that it's a social and political issue. (Ch 7, 158)
- Gallup poll (2011) showed men spent $11 more than women (159)
- Female-oriented is not good in of itself but like all products must be carefull evaluated (164)

The concept of financial confidence may be overrated. Men are usually the victims rather than women (168).

Olen says things can improve for women when financial services companies by giving women what they want-- low and transparent fees, clear explanations of products and advice, and lack of sales pressure.

---
<!-------------------------- CH 8 ---------------------------------->
## Ch 8 Who Wants to Be a Real Estate Millionaire? The Selling of Home Ownership as a Cure for Income and Investment Stagnation (172)

Olen saw the LA area come out of a housing crunch and was surprised 8 years later peaple halfway across the country were flipping homes and selling only to see it all reverse in "The greate3st economic calamity in 8 years" (ch 8, 174)

>There is something about real estate that makes us want it and want it bad _(Ch 9, 172)_

The expectation of home ownership is relatively new in American society (174).  

### History of Home Ownership in the US
- Before the Great Depression most rented. 
- Mortgages were only 3 - 5 years.  
- Franklin Roosevelt pioneered the 30 year mortgage to pump up the housing market
- When the Great Depression happened, people could no longer keep rolling over their short term mortgages (175)
- The GI Bill allowed ever more home owners. By 1950 for the first time ownership reached 50% (175).
- Home ownership to battle Red Scare according to Thomas Sugrue
- Purchasing a home is just an OK investment. It makes sense because of forced savings according to Sylvia Porter. (176)

Borrowing against equity = 2nd mortgage!

Look up Sylvia Porter _"Money Book"_
