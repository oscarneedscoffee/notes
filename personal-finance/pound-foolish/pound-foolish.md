# Pound Foolish

by Helaine Olen

<!-- TOC -->autoauto- [Pound Foolish](#pound-foolish)auto    - [Problems & Notes](#problems--notes)auto    - [Terms](#terms)auto    - [People](#people)auto    - [Chapter Summaries](#chapter-summaries)auto    - [Quotes](#quotes)auto    - [Look up:](#look-up)auto    - [References](#references)autoauto<!-- /TOC -->

## Problems & Notes

- Stock investments are not guaranteed. Bonds are safer
- Commission-free advisors must meet a **fiduciary duty** according to the SEC.  Brokers only need a suitability standard.
- **Variable annuities** (Ch. 5 p.104) are so complicated and difficult to understand that they only seem to make money for the companies selling them.
- Brokers use fear as a marketing tactic (Ch 5. 124)
- Barber & Odean recommend diversified index funds (Ch 6, 128). A 1999 study showed 70% short term traders lost money (129)
- Many of the pitches are are directed at seniors petrified of outliving their savings (Ch 6, 135)
- A problem with doomsday gurus like [Ruff and Dent Jr.](#people)  is over time they can lose their clients more money than the markets can on their own. (Ch 6, 142)

## Terms

- Financial doomsayers benefit from predicting armageddon because of the **recency effect**, the natural human bias to overemphasize the recent past over previous experience. (Ch 6, 139)
- Financial Regulation Authority (FINRA)
- Index Fund
- Behavioral Finance

<!------------------- PEOPLE ----------------------------------------->
## People

- **Theresa Ghilarducci**: critic of 401(k)’s
- **Carolyn McCarthy**: NY Dem who gets campaign Contributions from financial industry
- P**at Huddleston**: investment rights activist
- **Paul Issac:** hedge rund manager who advises against annuities (p. 117)⁃ Brad Barber & Terrance Odean: behavior finance experts, and business professors at UC (Ch6, 128)
- **Charles & Kim Githner** of The Money Show: advertisers in the pretext of financial advice (Ch 6 134-137)
- **Humberto Cruz** financial columnist
- **Howard Ruff and Harry Dent Jr.**  doomsday predictors who were one trick ponies (Ch 6, 141 - 142)
- **Mariko Chang** spent time studying women and wealth. Changr found that one is more risk averse if they have fewer assets, therefore, men take more risks than their lower paid female counterparts (Ch 7 166)

---

## Chapter Summaries

More chapter summaries [here](pf-chapter-summaries.md)


---
<!------------------------------ QUOTES ------------------------------->
## Quotes

> “ Annuities are, frankly, one of the most confusing financial products in existence” (ch5, 113-114)

>On Jorge Villar, president of Response Mail Express (RME):
> “… he is marketing a scheme that works by feeding the biases of human nature.”

> “Individual stock trading is a loser’s game” (ch 6, 129)


> “We’re not going to do due diligence for the investor. They have to do it themselves” Charles Githner The Money Show (137)

> “Almost all sellers of doom are excellent marketers” (Ch 6, 139)

## Look up:

- opensecrets.org : tracks political money
- US News and World Report called Ghiladucci “The Most Dangerous Woman in America”
- Robert Cialdini’s book, “Influence”
- CXO Advisory (Ch 6, 137) rated the advisors as less than impressive at the Money Show
- Barry Ritzholz: blogger and critic of doomsday predictors like Agora and The New Orleans Conference
- Bull! by Maggie Maher
- **Jane Bryant Quinn** author of "Making Most of Your Money". Olen name drops her in Ch 7 - 8.

<!---------------- REFERENCES --------------------------------------->
## References

- [The Retirement Gamble](http://www.pbs.org/wgbh/frontline/film/retirement-gamble/)
- Barber & Odean's "Boys Will Be Boys: Gender, Overconfidence, and Common Stock Investment" (Ch 7, 168)
