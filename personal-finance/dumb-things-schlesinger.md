# The Dumb Things Smart People Do with Their Money

- by Jill Schlesinger

## Dumb thing no. 1: You buy financial products you don't understand

5 Questions to ask anyone selling you financial products

1. How much will it cost?
2. What are the alternatives to this product?
3. How easy is it to get my money out of this investment? If I do, what fees or penalties would I pay?
4. What tax consequences will this financial product carry for me?
5. What's the worst case scenario I face with this product?

## Dumb Thing no 2: You take financial advice from the wrong people

- Look for a fiduciary
  - Is your adivisor legally obliged to put your interests first?
  - What professional certs do they have? CFP Board of Standards, NAPFA?
  - They ever been sanctioned for ethics>
  - Conflict of interest?
  