# Modernizing Legacy Applications in PHP
by Paul M. Jones (2016)

# Test Suites
use characterization tests, a test of what a system already does, not what it's supposed to do. Experiment with [Codeception](http://codeception.com/for/laravel)  

# Add autoloader
because of legacy code, author uses PSR-0 instead of PSR-4.