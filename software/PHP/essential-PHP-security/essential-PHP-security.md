# Essential PHP Security
**by Chris Shiftlett**

## Error Reporting 1.1.2

```php
    ini_set('error_reporting', E_ALL | E_STRICT);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', '/usr/local/apache/logs/error_log');
```

<http://php.net/exceptions>


## 1.3.3 Filter Input

- Generally speaking, it is more secure to consider data from session data stores and databases to be input, and this is the approach that I recommend for any critical PHP application.

## 1.3.4 Escaping Output

- Only escape filtered data.  The reason why is it forces you to filter the data too.

## Forms and Data 2.1
   
- 2 Types of data:  filtered and tainted
- Anything you created is considered trustworthy.
- Anything that originates from a remote source is input and all input is tainted 
- Forms provide an opening into your application for attack.  A user can send data to your application in 3 ways:  the url (GET data), content of a request (POST data), and HTTP header (a cookie).
- You can use a switch statement from a POST to make sure you only get a specific value.
    ```php    
    $clean = array(  );

    switch($_POST['color'])
    {
      case 'red':
      case 'green':
      case 'blue':
        $clean['color'] = $_POST['color'];
        break;
    }
    ```
- You can also distinguish between escaped, and unescaped characters
    ```php
    $html = array(  );

    $html['username'] = htmlentities($clean['username'],
      ENT_QUOTES, 'UTF-8');

    echo "<p>Welcome back, {$html['username']}.</p>";
    ```  
- Escaping MySQL
    ```php
    $mysql = array(  );

    $mysql['username'] =
      mysqli_real_escape_string($clean['username']);

    $sql = "SELECT *
            FROM   profile
            WHERE  username = '{$mysql['username']}'";

    $result = mysqli_query($sql);
    ```
	
- Avoid semantic attacks in POST or GET with sessions (2.2)


## Includes 5.1

- Place PHP include configuration outside of web root.  In fact, put as much PHP as possible outside the webroot.  Assume anything inside webroot to be public.


## Filename Manipulation 5.3

- Use basename() to get just the filename and not the entire path. http://us3.php.net/manual/en/function.basename.php


## Filtering data
- Use a whitelist approach to filtering data.  Treat it as an inspection process.
- You can store all filtered data in an array called $clean.  * Initialize $clean to be empty *  It may also be helpful to prevent any variables from a remote source to be named “clean”.

## Semantic attack

- <http://it.slashdot.org/story/03/05/08/122208/security-vulnerability-in-microsoft-net-passport>
- <http://stackoverflow.com/questions/732561/why-is-using-a-mysql-prepared-statement-more-secure-than-using-the-common-escape>
- <http://mattbango.com/notebook/code/prepared-statements-in-php-and-mysqli/>


## Resources

- [Semantic attack from Slashdot](http://it.slashdot.org/story/03/05/08/122208/security-vulnerability-in-microsoft-net-passport)
- [Hardened PHP](http://hardened-php.net/)
- Preparded Statments
    - <http://stackoverflow.com/questions/732561/why-is-using-a-mysql-prepared-statement-more-secure-than-using-the-common-escape>
    - <http://mattbango.com/notebook/code/prepared-statements-in-php-and-mysqli/>
