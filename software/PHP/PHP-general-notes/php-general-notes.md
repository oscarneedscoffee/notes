# General PHP notes

<!-- TOC -->

- [General PHP notes](#general-php-notes)
    - [General Tips](#general-tips)
    - [Forms:](#forms)
    - [MVC:](#mvc)
    - [Namespaces:](#namespaces)
    - [Traits](#traits)
    - [Essential PHP Security](#essential-php-security)
        - [Filtering data](#filtering-data)
        - [Error Reporting 1.1.2](#error-reporting-112)
        - [1.3.3 Filter Input](#133-filter-input)
        - [1.3.4 Escaping Output](#134-escaping-output)
        - [2.1 Forms and Data](#21-forms-and-data)
        - [5.1 Includes](#51-includes)
        - [5.3 Filename Manipulation](#53-filename-manipulation)
    - [My mistakes](#my-mistakes)
        - [Not quoting constants](#not-quoting-constants)
        - [Relative paths](#relative-paths)
        - [echo vs. print:](#echo-vs-print)
    - [Laravel Design Patterns & Best Practices](#laravel-design-patterns--best-practices)
        - [3 Groups of Patterns:](#3-groups-of-patterns)
        - [MVC architectural pattern](#mvc-architectural-pattern)
            - [What is a model?](#what-is-a-model)
            - [Purposes of the Model](#purposes-of-the-model)
        - [Model Instance](#model-instance)
            - [Fluent](#fluent)
        - [Eloquent](#eloquent)

<!-- /TOC -->

## General Tips

Use basename() to get filenames.  Strip out ‘..’ to prevent directory traversal in *nix
Use a whitelist approach

## Forms:

A user can send data to an application via GET, POST, or a cookie.

## MVC:

A good writeup on the Symfony site [Symfony versus flat PHP](http://symfony.com/doc/current/introduction/from_flat_php_to_symfony2.html)

## Namespaces:

`use` keyword is PHP’s import statement.

You can import functions and constants with 
> “use func …”
> “use constant …”

Some calsses are in a global namespace like PHP’s Exception class. To reference a globally namespace class inside another namespace prepend with “\”. 

> ex:  `$excp = new \Exception();`

## Traits

Traits act like classes, but look like interfaces you can use these to share functionality to completely differentiate

<!-- ***********  Essential PHP Security  ************** -->

## Essential PHP Security

### Filtering data

- Use a whitelist approach to filtering data.  Treat it as an inspection process.
- You can store all filtered data in an array called $clean.  *Initialize $clean to be empty*  It may also be helpful to prevent any variables from a remote source to be named “clean”.

### Error Reporting 1.1.2

```PHP
   <?php

    ini_set('error_reporting', E_ALL | E_STRICT);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', '/usr/local/apache/logs/error_log');

    ?>
```

<http://php.net/exceptions>

### 1.3.3 Filter Input

Generally speaking, it is more secure to consider data from session data stores and databases to be input, and this is the approach that I recommend for any critical PHP application.

### 1.3.4 Escaping Output

Only escape filtered data.  The reason why is it forces you to filter the data too.

### 2.1 Forms and Data 

- 2 Types of data:  filtered and tainted
- Anything you created is considered trustworthy.
- Anything that originates from a remote source isinput and all input is tainted 
- Forms provide an opening into your application forattack.  A user can send data to your application in 3ways:
  the url (GET data), content of a request (POST data) and HTTP header (a cookie).

> You can use a switch statement from a POST to makesure you only get a specific value.

```PHP
<?php

    $clean = array(  );

    switch($_POST['color'])
    {
      case 'red':
      case 'green':
      case 'blue':
        $clean['color'] = $_POST['color'];
        break;
    }

?>
```

> You can also distinguish between escaped, and unescaped characters

```PHP
<?php

    $html = array(  );

    $html['username'] = htmlentities($clean['username'],
      ENT_QUOTES, 'UTF-8');

    echo "<p>Welcome back, {$html['username']}.</p>";

    ?>
```

>Escaping MySQL

```PHP
    <?php

    $mysql = array(  );

    $mysql['username'] =
      mysqli_real_escape_string($clean['username']);

    $sql = "SELECT *
            FROM   profile
            WHERE  username = '{$mysql['username']}'";

    $result = mysqli_query($sql);

    ?>
```

Avoid semantic attacks in POST or GET with sessions (2.2)

### 5.1 Includes

Place PHP include configuration outside of web root.  In fact, put as much PHP as possible outside the webroot.  Assume anything inside webroot to be public.

### 5.3 Filename Manipulation

Use basename() to get just the filename and not the entire path. <http://us3.php.net/manual/en/function.basename.php>

Semantic Attack:
<http://it.slashdot.org/story/03/05/08/122208/security-vulnerability-in-microsoft-net-passport>

<http://stackoverflow.com/questions/732561/why-is-using-a-mysql-prepared-statement-more-secure-than-using-the-common-escape>

<http://mattbango.com/notebook/code/prepared-statements-in-php-and-mysqli/>

## My mistakes

Here are mistakes I make repeatedly, or annoyances I keep forgetting:

### Not quoting constants

I write `define(MY_CONSTANT_NAME, ‘some value’)`   It should be `define(‘MY_CONSTANT_NAME’, ‘some value’)`

### Relative paths 

can bite me in the ass. I should be more careful, and probably use absolute paths more.  I need to check on this.

### echo vs. print:

Print returns a 1 and is marginally slower.  You can use it in longer expressions.  Echo allows for coma seperated lists.

>Ex:  `echo $head, $feet, $torso`

Single quotes literally echo everything.  Double quotes evaluate expressons.
You can use echo to string together several values, but print only does one.

<!-- ***********  LARAVEL  ************** -->

## Laravel Design Patterns & Best Practices

by Yilmaz, Kilicdagi	

### 3 Groups of Patterns:

1. Creational:  serves to create objects.  “Program on the interfaces, not the implementation”
1. Structural:  Facilitates easy ways to communicate between entities
1. Behavioral:  Concerned with communications between objects

### MVC architectural pattern

![mvc](MVC.jpg)

- Allows you to reuse application logic.  Ex if you want to use a new external API
- If MVC followed you’ll only need to update the controller to render different templates / views

#### What is a model?

The model is the part that handles data management.  * The Model does not know where the data comes from and how it’s received

![model in MVC](model.jpg)

#### Purposes of the Model

- CRUD the data
- create conditional relations
- Monitor file I/O
- interact with third party web services
- handle caches & sessions

The model covers a huge percentage of the app’s logic in MVC.  There is a common mistake to confuse the model with **Model Instances**

### Model Instance
MIs are simple classes that mostly extend from the model layer. They separate the data logic for each section of your application

![model instance](model-instance.jpg)

> ex: Fetch a user from DB. You need a Validation layer on your Model instance to keep the Model from inserting malicious code in the DB. The MI validates and if reject, send message to Controller. The controller can then message the view.

Laravel does not use MVC directly, but does it through Model Intances. The Laravel Model structure focuses more on the database processes directly to access the DB, Laravel uses two classes, **Fluent** Query Builder & **Eloquent** ORM.

#### Fluent

is db agnostic, so if you change database, you shouldn’t have to change your queries much.

> Question: How does Fluent handle MS SQL?

Fluent uses PDO.

###  Eloquent

The Eloquent model of L4 is in `Framework\src\Illuminate\Database\Query`
