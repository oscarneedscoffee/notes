## resources

- Creating a simple data-driven [CRUD Microservice](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/multi-container-microservice-net-applications/data-driven-crud-microservice)
- eShopOnContainers a .Net Microservices Azure Kubernetes [sample project](https://github.com/dotnet-architecture/eShopOnContainers)
- Microservices [architecture style](https://docs.microsoft.com/en-us/azure/architecture/guide/architecture-styles/microservices)
- .Net Microservices Architecture for containerized [.Net applications](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/)
  