Azure uses the shared responsibility model.

- Private On Prem: Customer fully controls
- IaaS: 
	- Customer: Apps, runtimes, security, databases
	- MS:  Servers, virtualization, Server hardware storage, networking
- PaaS: Customer only controls the applications
- SaaS: MS mangaes it all. 

See the [[shared-responsibility-model.jpg]] for an easy to follow graphic.


[[division of security in the shared responsibility model]]


Notes from [[Mastering Azure Security]] 