No matter what service you have in the [[shared responsibility model]], **customers** always have the following security responsibilities:

-   Data governance and rights management
-   Endpoint protection
-   Account and access management


Microsoft's responsibilities are:

-   Physical data center
-   Physical network
-   Physical hosts

A graphic is in [[responsibility-zones.jpg]]


[[Mastering Azure Security]], Ch 1, *Exploring the shared responsibility model*