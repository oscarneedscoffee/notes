- Preconfigured with the [Core Rule Set](https://docs.microsoft.com/en-us/azure/web-application-firewall/ag/application-gateway-crs-rulegroups-rules?tabs=owasp32) from OWASP

## Resources
- Azure WAF intro from [MS](https://docs.microsoft.com/en-us/azure/web-application-firewall/ag/ag-overview)