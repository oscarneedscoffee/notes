## Resources

- [Containers vs. VMs](https://blog.netapp.com/blogs/containers-vs-vms/)
- [Helm, a k8s package manager](https://github.com/helm/helm)
- [Kubernetes: an overview](https://thenewstack.io/kubernetes-an-overview/)
- [Set up k8s locally with minikube](https://kubernetes.io/docs/setup/minikube/)
- [What is Kubernetes used for?](http://www.developintelligence.com/blog/2017/02/kubernetes-actually-use/)


