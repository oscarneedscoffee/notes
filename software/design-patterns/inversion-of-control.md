## Inversion of Control

From [Wikipedia](https://en.wikipedia.org/wiki/Dependency_injection)

> Dependency injection for five-year-olds.  
> 
>  When you go and get things out of the refrigerator for yourself, you can cause problems. You might leave the door open, you might get something Mommy or Daddy doesn't want you to have. You might even be looking for something we don't even have or which has expired.
> 
>What you should be doing is stating a need, "I need something to drink with lunch," and then we will make sure you have something when you sit down to eat. John Munsch, 28 October 2009
