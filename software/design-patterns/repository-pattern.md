# Repository Pattern

First mentioned by Eric Evans in *Domain-Driven Design*. A repository mimics a collection without having to know how something is persisted by having a common _interface_. You can think of a repository as in-memory storage.

If you change how you implement persistence, the pattern will allow you to keep your application logic the same, and all you have to do is change the repository.


## Links

- [Tutsplus explanation](https://code.tutsplus.com/tutorials/the-repository-design-pattern--net-35804)
- [Repository Pattern done right](http://blog.gauffin.org/2013/01/repository-pattern-done-right/)
- [basics](http://shawnmc.cool/the-repository-pattern)

## Books
- *Domain Driven Design*
- *Domain-Driven Design in PHP*


## Laravel

- [Laravel Repository](http://vegibit.com/laravel-repository-pattern/)
- [Laravel 5](https://dericcain.com/using-the-repository-pattern-in-laravel-5-f7c6388db8d0)
- [Using Doctrine and Laravel](https://code.tutsplus.com/tutorials/the-repository-pattern-in-laravel-5--cms-25464)
