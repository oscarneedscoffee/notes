# C\# 

## Scratch pad and brain dump

- C\# allows nullable types. append it with a `?`. e.g. `string Method(string x, string? y)`
- Primitives are really structs all derived from class *Object*
- Understand 
- A DLL is like a class without a main function

---

## [Boxing and unboxing](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/boxing-and-unboxing) 

Boxing is the implicit conversion of a type into an oject. Unboxing is the explicit conversion of the object type to another type.  
Example of implicit boxing:  
```c#
int i = 123;
object o = i; //implicit
```

You can box explicity, but it is not required
```c#
int i = 123;
object o = (object)i  //explicit
```

---
<!--****************************       Placeholder notes for J Skeet book  ****************************  -->

## C\# in Depth 4th ed. by John Skeet

- [Copy of book's source](https://github.com/ocpineda/learn-c-sharp/tree/master/c-sharp-in-depth-4thEdition)
- [C# in depth website](https://csharpindepth.com/)
- Check out his [Noda Time site](https://nodatime.org) and his [Github](https://github.com/nodatime/nodatime)

### Ch 2
#### Generics
Generics make it so you don't have to know the size of an array or any collection beforehand. They have supplanted using class ArrayList. The advantage of generics is you get type errors at compile time rather than run time.  


---

## C\# Specification

- [Github](https://github.com/dotnet/csharplang/blob/master/spec/introduction.md) 
- [C\# 6 spec](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/introduction)

---

## Udemy Course

[Udemy course](https://www.udemy.com/course/the-complete-c-sharp-developer-course) with Ahmad Mohey

### Compiler vs. Interpreters

- compilers: src -> bytecode. Takes entire program as input. Requires more memory
- Interpreters: single line of code. Less analysing but slower execution. Requires less memory

### .Net Core

It's a re-implementation of .Net Framework

---
## Resources

- [My own learning C# project on Github](https://github.com/ocpineda/learn-c-sharp)
- [My Entity Framework notes](entity-framework.md)
- [.Net API Browser](https://docs.microsoft.com/en-us/dotnet/api/?view=netframework-4.8)
- [9 C# coding standards Developers need](https://blog.submain.com/coding-standards-c-developers-need/)
- [.Net Fiddle](https://dotnetfiddle.net)
- [C# 6.0 language spec](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/introduction)
- [My list of Visual Studio shortcuts](https://github.com/ocpineda/cheat-sheets/blob/master/visual-studio.md)

### My notes from other resources

- [Pro C# 7 with .Net and .Net Core, Japiske and Troelsen](booknotes/pro-c-sharp-7.md)
- [Entity Framework](entity-framework.md)
- [Hands on Architecture book notes](booknotes/hands-on-architecture-c-sharp.md)