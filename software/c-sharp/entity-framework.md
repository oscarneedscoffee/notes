## General

How EF fits in your application ( [Entity Framework Tutorial](https://www.entityframeworktutorial.net/what-is-entityframework.aspx) )  

<img src="entity-framework/ef-in-app-architecture.png" height="300px">

Three models: conceptual, storage, and mappings. The EF API converts Linq-to-entities into SQL using the entity domain model (EDM). The EF API does insert, update, and delete when `SaveChanges()` is called ( [ef tutorial](https://www.entityframeworktutorial.net/basics/how-entity-framework-works.aspx) ).


## Terms

Entity:

Entity Domain Model (EDM) - An in-memory representation of the metadata. ( [see more](https://www.entityframeworktutorial.net/basics/how-entity-framework-works.aspx) )

## Resources

- [Overview](https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/ef/overview)
- [Entity Framework tutorial](https://www.entityframeworktutorial.net/)
- [Domain Driven Design best practices from Microsoft](https://msdn.microsoft.com/en-us/magazine/dd419654.aspx)

