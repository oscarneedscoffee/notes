## General 

**"Pro C# 7 with .Net & .Net Core"** by Japiske & Troelson  

C# needs the .net runtime. The code targeting the runtime is called *managed code*. The binary is called an *assembly*.

>Code that can't be hosted by the .net runtime is unmanaged code.

- .Net binaries may have the same file extension as unmanaged Win binaries (.dll or .exe), but no internal similarities
- Assemblies are described using metadata called *manifests*. There are tools to examine the manifest. `idlsm.exe` can be used to view assemblies.
- CIL code is platform dependent so is compiled on the fly with a just in time compiler

## Common Type System (CTS)

A type is a general term to refer to a member of set `{class, interface, delegate, structure, enumeration}`

- A *struct* is a value type with value semantics.  \* TODO *lookup WTF is value semantics* \*
- *Enumerations*  for key value pairs. Holds 32 bit int by default. CTS demands from common base system enum.
- *Delegates* are similar to C pointers except they derive from class `System.MulitcastDelegate`

## TODO

- Look up
  - [ ] attributes
  - [ ] generics
  - [ ] anonymous functions
  - [ ] partials
  - [ ] lambda
  - [ ] dynamic keyword
  - [ ] asnync/await <-- for multi-threaded and async programming