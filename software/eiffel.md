## Eiffel Object Oriented Software Construction

Inspiration for Eiffel came from a compendium *“Structured Programming”* by Dahl, Djikstra, and Hoare

## Design by contract: 
 preconditions and postconditions.  “Code should fail hard.  It shouldn’t try to verify contract conditions”.  Applies to preconditions, responsibilities with someone else like automated code for testing and debugging.

Rewrite of his book with Claude Baudoin “Programming Methodology” and a terrible data structures class he had to teach in C lead to Eiffel.

Chain between design and development is continuous.  It’s seamless development

Eiffel has less conventions.  Scoop model of concurrency and baggage.  