# Test Driven Development
by Kent Beck

<!-- TOC -->

- [Test Driven Development](#test-driven-development)
    - [Red Bar Pattern (ch 26)](#red-bar-pattern-ch-26)
        - [Starter Test (134)](#starter-test-134)
        - [One Step Test](#one-step-test)
        - [Another Test](#another-test)
    - [Regression Test](#regression-test)

<!-- /TOC -->

## Red Bar Pattern (ch 26)

- One Step test
- Starter test
- Explanation test
- learning test
- Another test

> **Feedback Loop**  
> *Red/Green/Refactor*    
>
> For the R/G/R loop, choose inputs and outputs that are trivially easy to discover. (134)

### Starter Test (134)

- Easy input & output
- make it quick & simple

### One Step Test

- Each step should represent one step toward your overall goal.
- Make a list and pick the one you can implement
- A good metaphor to use is known to unknown

### Another Test

If during testing you get an idea, write it down for later. This will help you stay focused

## Regression Test

- Write the smallest piece of code that will fail.
- Regression tests are tests you would have written when you originally wrote the code.
- RT at the application level will give customers a chance to speak concretely about what is wrong and what they expect
- At the small scale, RT helps improve testing.

>**TODO**
> Get a clearer understanding of R/G/R

Take a break when your are blocked (138). Sometimes if a break isn't enough, Do Over (139)