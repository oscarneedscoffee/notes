## Glossary

- [reverse proxy](ms-definitions.md#reverse-proxy)
- service discovery
- [service mesh](ms-definitions.md#service-mesh)
- side car


## References
- [Microservices at Netflix](https://www.nginx.com/blog/microservices-at-netflix-architectural-best-practices/)
- [Nginx Microservce architecture](https://www.nginx.com/blog/introducing-the-nginx-microservices-reference-architecture/)

## Interesting Articles
- [Consul vs. Eureka](https://www.consul.io/intro/vs/eureka.html)
- Tackling Business Complexity in a [Microservice with DDD and CQRS]()

