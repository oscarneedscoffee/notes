## Reverse Proxy
a type of proxy server that retrieves resources on behalf of a client from one or more servers. 

## Service Mesh
From the [Wikipedia](https://en.wikipedia.org/wiki/Microservices) page for microservices is this definition of a service mesh:

>"In a service mesh, each service instance is paired with an instance of a reverse proxy server, called a service proxy, sidecar proxy, or sidecar. The service instance and sidecar proxy share a container, and the containers are managed by a container orchestration tool such as Kubernetes. The service proxies are responsible for communication with other service instances and can support capabilities such as service (instance) discovery, load balancing, authentication and authorization, secure communications, and others."
