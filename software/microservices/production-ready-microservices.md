# Production Ready Microservices
by Susan Fowler

- the organizational structure of a company adopting microservices must be radically changed to support microservice architecture
- Scalability requires concurrency and partitioning: the two things that are difficult to accomplish with a monolith.

---

## Ch1 

<img src="modern-app.png" alt="pieces of modern apps"  width="600px"/>

### 3 Ways the pieces of modern apps can combine to make one application

1. Front and backend in a single code base and another DB
2. Front and back separate codebases and another DB
3. If no external DB required, put all three into single codebase

Vertical scaling & horizontal scaling. See fig 1-2, and 1-3

Apps tend to become **monoliths** as they scale.

### Problems with monoliths

- Development becomes difficult
- Testing is a burden
- Technical debt adds up

Why does this happen?
The nature of monoliths is directly opposed to scalability.

To scale you need **concurrency** and **partitioning**. One process will pick up one task at a time. Partiioning does divide and conquer. Microservices let you do it horizontally which is difficult with monoliths.

The best way to scale an application is to break it down into many smaller applications that do just one task.

**What is an microservice?** A small app only one thing and it does it well. It's a small component, and is easily replaceable, independently developed, and deployed.

The main difference between monolith and microservice: A monolith contains **all** features in **one** code base, and all deployed at the same time. Each server on the horizontal must host a complete copy of the entire app (see figure 1-4).

<!------------------- CH2 -------------------------------------------------------------->
## Ch2 Production Readiness

### Four layers of a microservice
4. Microservices
3. Application platform
2. Communication
1. Hardware

#### Communication.
Two communication paradigms are HTTP + REST/THRIFT, and messaging.  

HTTP+REST is easy and reliable, but synchronous (blocking).  Messaging comes in two main flavors ***publish-subscribe***, and ***request-response***