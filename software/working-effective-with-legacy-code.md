# Working Effectively with Legacy Code
by Michael Feathers  

 ## Ch 2 Working with Feedback

 Two primary ways a system can change
 1. Edit and pray
 2. Cover and modify

 Edit and pray is when you make very careful changes, and seems like "working with care". Cover and modify is like working with a safety net. You cover it with tests

