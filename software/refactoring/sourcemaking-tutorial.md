# Refactoring

Notes taken from [Source Making](http://sourcemaking.com) at <https://refactoring.guru> 

<!-- TOC -->

- [Refactoring](#refactoring)
    - [I. Long Method](#i-long-method)
        - [Recipe 1: Extract Method](#recipe-1-extract-method)
        - [Recipe 2: Reduce Local Variables before extracting a method](#recipe-2-reduce-local-variables-before-extracting-a-method)
            - [2a. Replace Temp with Query](#2a-replace-temp-with-query)
            - [2b. Introduce parameter object](#2b-introduce-parameter-object)
            - [2c. Preserve Whole Object](#2c-preserve-whole-object)
        - [Recipe 2: Replace Method with Method Object](#recipe-2-replace-method-with-method-object)

<!-- /TOC -->

## I. Long Method 

Reduce your method sizes with the _extract method_, 

### Recipe 1: Extract Method

If you have a code fragment that can be grouped together, it shoudld probably be its own function. 

`* ` Hint: _if you need comments to explain that fragment, it may need extracting_


**Problem**
```java
void printOwing() {
  printBanner();

  //print details
  System.out.println("name: " + name);
  System.out.println("amount: " + getOutstanding());
}
```

**Solution**
```java
void printOwing() {
  printBanner();
  printDetails(getOutstanding());
}

void printDetails(double outstanding) {
  System.out.println("name: " + name);
  System.out.println("amount: " + outstanding);
}
```


### Recipe 2: Reduce Local Variables before extracting a method

If local variables and parameters interfere with extracting a method, use 

  - [Replace Temp with Query](#2a-replace-temp-with-query)
  -  [Introduce Parameter Object](#2b-introduce-parameter-object)
  - [Preserve Whole Object](#2c-preserve-whole-object)


#### 2a. Replace Temp with Query

You are using a temporary variable to hold the result of an expression.

_Extract the expression into a method. Replace all references to the temp with the new method. The new method can then be used in other methods._

**Problem**

```java
double calculateTotal() {
  double basePrice = quantity * itemPrice;
  if (basePrice > 1000) {
    return basePrice * 0.95;
  }
  else {
    return basePrice * 0.98;
  }
}
```

**Solution**

```java
double calculateTotal() {
  if (basePrice() > 1000) {
    return basePrice() * 0.95;
  }
  else {
    return basePrice() * 0.98;
  }
}
double basePrice() {
  return quantity * itemPrice;
}
```

The problem with temps is that they are temporary and local. Because they can be seen only in the context of the method in which they are used, temps tend to encourage longer methods, because that’s the only way you can reach the temp. By replacing the temp with a query method, any method in the class can get at the information. That helps a lot in coming up with cleaner code for the class. (Refactoring, Fowler)


#### 2b. Introduce parameter object

**Problem**  
Your methods contain a repeating group of parameters

```java
amountInvoicedIn(start: Date, end: Date)
amountReceivedIn(start: Date, end: Date)
amountOverdueIn(start: Date, end: Date)
```

**Solution**  
```java
amountInvoicedIn(date: DateRange)
amountReceivedIn(date: DateRange)
amountOverdueIn(date: DateRange)
```

#### 2c. Preserve Whole Object

(Fowler, Ch 10.)

**Problem**  
You get several values from an object and then pass them as parameters to a method.

```java
int low = daysTempRange.getLow();
int high = daysTempRange.getHigh();
boolean withinPlan = plan.withinRange(low, high);
```

**Solution**  
Instead, try passing the whole object.

```java
boolean withinPlan = plan.withinRange(daysTempRange);
```

### Recipe 2: Replace Method with Method Object

You have a long method in which the local variables are so intertwined that you cannot apply Extract Method.

**Problem**
```java
class Order {
  //...
  public double price() {
    double primaryBasePrice;
    double secondaryBasePrice;
    double tertiaryBasePrice;
    // long computation.
    //...
  }
}
```

**Solution**  
Transform the method into a separate class so that the local variables become fields of the class. Then you can split the method into several methods within the same class.

```java
class Order {
  //...
  public double price() {
    return new PriceCalculator(this).compute();
  }
}

class PriceCalculator {
  private double primaryBasePrice;
  private double secondaryBasePrice;
  private double tertiaryBasePrice;
  
  public PriceCalculator(Order order) {
    // copy relevant information from order object.
    //...
  }
  
  public double compute() {
    // long computation.
    //...
  }
}
```