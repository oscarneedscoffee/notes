## About
- last edited 20180820
- Most of these notes are from Mulloy's Web API Design book

## Braindump
- Keep your API as simple as possible!
- HTTP is stateless
- APIs need caching, rate-limiting, and authentication
- rate-limiting help prevent DDOS
- Authentication is done with tokens, cross realm, and http-digest, etc..
  - tokens have to be given with every request
- For attributes, use JSON as the default, and stick to JS conventions for attribute names
- Consolidate API requests under one API subdomain e.g. `api.twitter.com` (Mulloy, 23)
- Use OAuth 2.0


## Nouns and verbs
- Use nouns for urls, not verbs
- For API calls that send responses that are not resources, use verbs. Examples of actions are Calculate, Translate, Convert

e.g. An API to convert 100 euros to Chinese Yen  
`/convert?from=EUR&to=CNY&amount=100`

Make it clear in your API documentation that these "non-resource" scenarios are different (Mulloy 19)

## Versioning
- Use version numbers, but keep it simple. Don't use dot notation (e.g. v1.2.3) because APIs don't need that level of granularity
  - Maintain at least one version back

## Error Handling

### Error Handling: Standard Behavior
- For error handling, use HTTP codes that map well to the actual error. You don't need all 70 status codes, but at least the main ones like 200, 300, 400, 500
  - the outcomes for status codes are
    1. Everything worked - success
    2. The app did something wrong - client error
    3. The API did something wrong - server error

### Error Handling: Exceptional behavior 
Sometimes clients can't handle anything other than 200 OK. You need a way for the app developer to intercept an error code.
Twitter handles this by using an optional parameter *suppress_response_codes* 

```TEXT
/public_timelines.json?
suppress_response_codes=true
HTTP status code: 200 {"error":"Could not authenticate you."}
```

## Pagination & partial responses
Partials allow you to send just the data developers need instead of the full object. Use partials in a comma delimited list like below.

Here is an example from **Facebook**  
`/joe.smith/friends?fields=id,name,picture`

For pagination, use *offset* and *limit* terms in your query string since it is intuitive for developers familiar with those keywords in many databases

e.g. `/dogs?limit=25&offset=50`  

## SDK
Complement your API with an SDK.
[Yahoo!](https://developer.yahoo.com/social/sdk/)
[Paypal](https://developer.paypal.com/docs/api/rest-sdks/)

## API Facade Pattern (Mulloy, 32)

Backend systems are often complicated, legacy systems that are difficult to expose to HTTP. Before thinking about
the API Facade Pattern, recognize some anti-patterns

- The Build Up Approach
- Standards Committee Approach
- The Copy Cat Approach

An API Facade is an abstraction layer between your the UI, and the API implementation. \*see GOF book on facade patterns 

## Resources
- [My HTTP notes](https://gitlab.com/oscarneedscoffee/notes/blob/master/software/http-developers-handbook.md)
- [Microsoft REST API Guidelines](https://github.com/Microsoft/api-guidelines/blob/master/Guidelines.md)
- [API Craft Google group](https://groups.google.com/forum/?fromgroups#!forum/api-craft)
- [Roy Fielding's REST dissertation](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm)

## Read later
- [Treehouse using Github API with PHP](https://teamtreehouse.com/library/using-the-github-api-with-php)


## Bibliography

- *Web API Design: Crafting Interfaces that Developers Love* by Brian Mulloy
