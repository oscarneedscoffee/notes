# Cyber Security


## to do
- [ ] Check out <https://www.hackers-arise.com/getting-started> mentioned in *Linux Basics for Hackers* book by OccupyTheWeb

## Notes/ brain dump

- OSI layers mnemonic: Please Do Not Throw Shrimp Po-boys Away 
- IBM [Call for Code](https://developer.ibm.com/callforcode]
- Shona Brown, led Google to operational excellence
- reading foundations of information security by Jason Andress 
- Parkerian Hexad CIA-UPA
- <https://public-apis.xyz/>
- NIST 800-53 standard for compliance
- <https://www.shodan.io/>
- McManus from iDefense 
  


## Resources

- [Additional resources](security-resources.md)
- [My CompTIA Sec+ notes](security-plus/security-plus-exam.md)
- [Cybrary free cyber security site](https://www.cybrary.it/)
- [Secure Coding to Prevent Vulnerabilities](https://insights.sei.cmu.edu/sei_blog/2014/05/secure-coding-to-prevent-vulnerabilities.html)
- [definitions](sec-definitions.md)


