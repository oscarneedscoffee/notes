## Advanced Persistent Threat
[APT](https://en.wikipedia.org/wiki/Advanced_persistent_threat) is stealth threat actor (usually a nation state) with access for a long period of time.

## Business Impact Analysis
BIA - Business Impact Assessment. Determines how large is the large if the application if it is down or not available. Determines what resources and technology are necessary for this application

## NIST 
[**National Institute of Standards and Technology**](https://www.nist.gov/topics/cybersecurity). A national standard for security and globally recognized industry standard

## Open Source Intelligence
OSINT is the use of social media, and Google to gather publicly available info.