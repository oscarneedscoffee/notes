# CompTIA All in One SY0-501 by Conklin, Cothren, et al

- [CompTIA All in One SY0-501 by Conklin, Cothren, et al](#comptia-all-in-one-sy0-501-by-conklin-cothren-et-al)
  - [Ch 1](#ch-1)
    - [5 Types of Root Kits](#5-types-of-root-kits)
    - [Remote Access Trojan (RAT)](#remote-access-trojan-rat)
    - [Viruses vs. Worms](#viruses-vs-worms)
    - [Logic Bomb](#logic-bomb)
    - [Indicators of compromise (IOC)](#indicators-of-compromise-ioc)
    - [IOCs resources](#iocs-resources)
  - [Ch 2 Attacks](#ch-2-attacks)
    - [Social Engineering](#social-engineering)
    - [Phishing](#phishing)
    - [3rd Party Authorization](#3rd-party-authorization)
    - [Helpdesk / Tech:](#helpdesk--tech)
    - [Dumpster Diving](#dumpster-diving)

## Ch 1

### 5 Types of Root Kits

1. Firmware
2. Virtual
3. Kernel
4. Library
5. Application Level

Because root kits are so invasive, most sysadmins will do a re-install from the last known clean image.

### Remote Access Trojan (RAT)

Malware with an operator behind it

### Viruses vs. Worms

Think of viruses for applications, and worms for networks. Worms are self-propagating

### Logic Bomb

Usually installed by an authorized user (making it harder to detect). One set for a specific date time is a *time bomb*  The threat of logic bombs necessitates separation of duties, periodic review of a system, and an active backup.

---

### Indicators of compromise (IOC)

IOCs act as bread crumbs for the incident response investigator. A tool like YARA can list signatures of IOCs

### IOCs resources

- OpenIOC
- STIX/ TAXII/ CybOx

---

## Ch 2 Attacks

Attacks can be social applications, or network. Attacks will affect at bese one of the security requirements of CIA: Confidentiality, Integrity, and Availability

Attacks can be grouped into:

1. Attacks on specific software 
2. Attack on a specific protocol or service


### Social Engineering

- Will play to stereotypes or whatever it takes to win a person over.
- Best defense is comprehensive training and verifying

### Phishing

- **Spear fishing:** targeted to a group
- **Whaling:** High value target
- **Vishing:** Voice attacks. Fake calls

A countermeasure to piggybacking is a mantrap-- using 2 sets of doors.

### 3rd Party Authorization

Uses previously obtained info to appear as a helpful 3rd party.

### Helpdesk / Tech: 

Can Pose as customer to attack help desk, or pose as help desk to attack customer

### Dumpster Diving

Little enforcement because trash is no longer private property