# CompTIA Sec+ SYO-501 (2019)

Text is **CompTIA Security+ All-in-One Exam** by Davis, Williams, et al  
McGraw-Hill 2018


## Ch 2 Attacks

- DOS: 
    - **Syn Flooding Attack**: attacker keeps sending syn from different non-existent IPs. While target is waiting for a response, its queue gets full and other SYNs from legit nodes will be locked out. Protect against SYN by setting a time-out for TCP connections.
    - **Ping of Death** (POD): Ping packet should not be more than 64KB. Some systems can't handle it and will crash.
- DDOS: A networked attack from many different systems. Patching systems is one way to fight DDOS. You can use AV to prevent your system from getting used. You can also block ICMP at the border.
- Man-in-the-Middle
- Buffer Overflows
- Cross-Site Scripting
  - Non-persistent
  - Persistent
  - DOM-based
- Watering Hole Attack. Infecting a target website and wait for victims to visit like predator at a watering hole. Difficult to do so usually by nation state


### Social Engineering
Social engineering is successful because people want to help when asked questions (unknowingly giving out information), and people want to avoid confrontation. [video showing how hackers use social engineering](https://youtu.be/lc7scxvKQOo)

The tools of social engineering:  
- Authority: act like a boss. Best defense is strong policies
- Intimidation: 
- Consensus: usually through rounds of group negotiation
- Scarcity: rush decision because of limited supply
- Familiarity: Misplaced trust by building perception of familiarity. 
- Trust: shape perception so target is lead to feel they’re doing the right thing
- Urgency: rush into a decision