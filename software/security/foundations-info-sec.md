Cybersec brain dump

- Check out censys.io
- Blogs https://www.rasmussen.edu/degrees/technology/blog/top-cyber-security-blogs/

## Defense in depth

layers  and defensive measures. Taken from [Foundations of Information Security](https://nostarch.com/foundationsinfosec) *(Andress, 2019)*

### External network 	
- DMZ 
- VPN
- Logging
- Auditing
- Penetration testing
- Vulnerability analysis

### Network perimeter
- Firewalls
- Proxy
- Logging
- Stateful packet inspection
- Auditing
- Penetration testing
- Vulnerability analysis 

### Internal network  
- IDS
- IPS
- Logging
- Auditing
- Penetration testing
- Vulnerability analysis

### Host
- Authentication
- Antivirus
- Firewalls
- IDS
- IPS
- Passwords
- Hashing
- Logging
- Auditing
- Penetration testing
- Vulnerability analysis

###  Application
- SSO
- Content filtering
- Data validation
- Auditing
- Penetration testing
- Vulnerability analysis

### Data
- Encryption
- Access controls
- Backups
- Penetration testing
- Vulnerability analysis

