## Podcasts and news

- [Cyberwire Daily](https://thecyberwire.com/podcasts/daily-podcast)
- [Krebs on Security](https://krebsonsecurity.com/)
- [OWASP Cheatsheets](https://cheatsheetseries.owasp.org/)
- [OWASP Chaos Engineering](https://dsomm.timo-pagel.de/detail.php?dimension=Implementation&subdimension=Infrastructure+Hardening&element=Usage+of+a+chaos+monkey)
- [Packet Pushers](https://packetpushers.net/) has several podcasts. I like Heavy Strategy, and Heavy Networking
- [Risky Business Podcast](https://risky.biz/)
- [Sans Internet Stormcast](https://isc.sans.edu/): daily cyber news
