## OIDC

- https://www.cloudflare.com/learning/access-management/authn-vs-authz/
- https://www.scottbrady91.com/OAuth/OAuth-is-Not-Authentication
- https://www.cloudidentity.com/blog/2018/04/20/clients-shouldnt-peek-inside-access-tokens/
- http://www.thread-safe.com/2012/01/problem-with-oauth-for-authentication.html
- https://www.scottbrady91.com/OAuth/Why-the-Resource-Owner-Password-Credentials-Grant-Type-is-not-Authentication-nor-Suitable-for-Modern-Applications
- https://www.scottbrady91.com/Blockchain-Identity/Technical-Review-of-Civics-Secure-Identity-Platform
- https://developer.okta.com/blog/2017/06/21/what-the-heck-is-oauth
- https://www.pingidentity.com/en/resources/client-library/articles/openid-connect.html
