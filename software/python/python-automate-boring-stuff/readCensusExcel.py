#! python3
# readCensusExcel.py

"""
1. Reads the data from the Excel spreadsheet
2. Counts the number of census tracts in each county
3. Counts the total population of each county
4. Prints the results

The code:
1. Open and read the cells of an Excel document with the openpyxl module.
2. Calculate all the tract and population data and store it in a data structure.
3. Write the data structure to a text file with the .py extension using the pprint module.

"""

import openpyxl, pprint # pprint to write to text file

wb = openpyxl.load_workbook('censuspopdata.xlsx')
print('opening workbook...')

sheet = wb['Population by Census Tract']
countryData = {} #dictionary

# TODO Fill in countyData with each county's population and tracts.
print("Reading rows...")


for row in range(2, sheet.max_row + 1):
    # Each row in the spreadsheet has data for one census tract.
    state  = sheet['B' + str(row)].value
    county = sheet['C' + str(row)].value
    pop    = sheet['D' + str(row)].value

# TODO: Open a new text file and write the contents of countyData to it.