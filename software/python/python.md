# Python Tutorial

## Keywords

  |  |  |  |  | 
-------  | ------- | ------- | ------- | -------
and      | del     | from    | not     | while
as       | elif    | global  | or      | with
assert   | else    | if      | pass    | yield
break    | except  | import  | print   |
class    | exec    | in      | raise   |
continue | finally | is      | return  |
def      | for     | lambda  | try


## Dictionaries

A dictionary is like an associative array
`a = {"a":"hi", "b":"bye", 5:"hello"}`

<!-- ********** LISTS ********** -->
## Lists

```python
a = ['a', 'b', 'c', 5, 'spam', 5999, 'woot'] 
a[-2] # returns 5999
```

### Extend and Append

```python
a[1:3]  # prints ['b', 'c']
a[2:]   # ['c', 5, 'spam', 5999, 'woot']
a[:3]   # ['a', 'b', 'c']
a[:]    # prints everything
```

`*` notice for the right side it doesn't print that particular index position.

extend **concatenates** a list to the end as the last element

- if a has 3 elements and you append b which has 3 elements, a will now have 6 elements.  
- If you extend a with b,a will now have 4 elements
- when you extend n lists it adds list(a) + list(b) elements to list a
- when you append n lists it will add list(a) + 1 to list a

### Slicing Lists

```python
>>> li
['a', 'b', 'mpilgrim', 'z', 'example']
>>> li[:3] 1
['a', 'b', 'mpilgrim']
>>> li[3:] 2 3
['z', 'example']
>>> li[:] 4
['a', 'b', 'mpilgrim', 'z', 'example']

>>> li
['a', 'b', 'mpilgrim', 'z', 'example']
>>> li[:3] 1
['a', 'b', 'mpilgrim']
>>> li[3:] 2 3
['z', 'example']
>>> li[:] 4
['a', 'b', 'mpilgrim', 'z', 'example']
```

1. If the left slice index is 0, you can leave it out, and 0 is implied. So `li[:3]` is the same as `li[0:3]` from Example 3.8, _“Slicing a List”_.
1. Similarly, if the right slice index is the length of the list, you can leave it out. So `li[3:`] is the same as `li[3:5]`, because this list has five 
elements.
1. Note the symmetry here. In this five-element list, `li[:3]` returns the first 3 elements, and `li[3:]` returns the last two elements. In fact, `li[:n]` will always return the first n elements, and li[n:] will return the rest, regardless of the length of the list.
1.  If both slice indices are left out, all elements of the list are included. But this is not the same as the original li list; it is a new list that 
happens to have all the same elements. `li[:]` is shorthand for making a complete copy of a list. 

<!-- ***********  Tuples  ************** -->
## TUPLES  

- A tuple is an immutable list. A tuple can not be changed in any way once it is created.
- A tuple is defined in the same way as a list, except that the whole set of elements is enclosed in parentheses instead of square brackets.
`a = ("a", "b", "c", 5, "spam", 5999, "woot")`
- when you slice a list you get a new list.  When you slice a tuple you get a new tuple.
- Tuples have no methods.  You can't append, extend, delete, or index a tuple.  You can however use 'in' to see if an element exists in the tuple.
    >ex:   "c" in a   #returns true
    >       "wazooo" in a   #returns false
- tuples are faster than lists, they are write protected, tuples can be used as keys in a dictionary.
- Tuples are used in string formatting.
- Tuples can be converted into lists, and vice-versa. The built-in tuple function takes a list and returns a tuple with the same elements, and the list function takes a tuple and returns a list. In effect, tuple freezes a list, and a list thaws a tuple.

## DECLARING VARIABLES 3.4
line continuation is done with the backslash '\'
Assigning Multiple Values at Once 3.4.2

```python
>>v = ('a', 'b', 'e')
>>(x, y, z) = v     
x
     'a'
y
     'b'
z
     'e'
```

- You can use the range() function to assign consecutive values.  It's enum in C
- notice in the assignment the lack of quotes in x, y, z

## FORMATTING STRINGS 3.5

```python
>>k = "uid"
>>v = "sa"
>>"%s=%s" % (k, v) 
   'uid=sa'
```
You can also concatenate but string concatenation only works for other strings
When defining a tuple with one element the syntax is:  (userCount, )

## FORMATTING NUMBERS

```python
>>print "Today's stock price: %f" % 50.4625   
   50.462500
>>print "Today's stock price: %f" % 50.4625   
   50.462500
>>print "Change since yesterday: %+.2f" % 1.5 
   +1.50
```

## MAPPING LISTS 3.6

```python
>>li = [1, 9, 8, 4]

>>[elem*2 for elem in li]      #1
  [2, 18, 16, 8]
>>li               # 2
   [1, 9, 8, 4]
>>li = [elem*2 for elem in li] #3
>>li
  [2, 18, 16, 8]
```

1. To make sense of this, look at it from right to left. li is the list you're mapping. Python loops through li one element at a time, temporarily assigning the value of each element to the variable elem. Python then applies the 
function `elem*2` and appends that result to the returned list.
1. Note that list comprehensions do not change the original list.
1. It is safe to assign the result of a list comprehension to the variable that you're mapping. Python constructs the new list in memory, and when the list comprehension is complete, it assigns the result to the variable.


## Joining Lists and Splitting Strings 3.7

everything is an object including strings. `"some_string".join(some_other_object)`
split does the opposite. split() takes a second argument that tells you how many times to split. anything.split(delimiter, 1) is useful when you want to search a string for a substring and then work with everything after that substring


## Getting Object References with getattr 4.4

You can get a reference to a function without knowing its name until run-time by using the getattr function.

```python
>>> li = ["Larry", "Curly"]
>>> li.pop 
<built-in method pop of list object at 010DF884>
>>> getattr(li, "pop") 
<built-in method pop of list object at 010DF884> 
>>> getattr(li, "append")("Moe") 
>>> li
["Larry", "Curly", "Moe"]\
```

`** `The return value of getattr is the method itself. The last example above is the same as `li.append("Moe")`. Instead of calling the method directly the function can be specified as a string
getattr as a Dispatcher
getattr can take a third parameter which acts as a default value. This is very useful for introspection

## Filtering Lists 4.5

The list filtering syntax is [mapping-expression for element in source-list if filter-expression]
It's just an extension of list comprehensions from section 3.6 [mapping-expression for element in source]  
`>>> li = ["a", "mpilgrim", "foo", "b", "c", "b", "d", "d"]`  
`>>> [elem for elem in li if len(elem) > 1]`  
and and or in Python 4.6

- In Python, and and or perform boolean logic as you would expect, but they do not return boolean values; 
instead, they return one of the actual values they are comparing.
- ` 0, '', [], (), {},` and None are false in a boolean context; everything else is true.
- When the expression is true, python returns the last true value. When the expression is false, python returns the first false value.
- and true = return last true, false = return first false
- or true = return first true, false = return last false
- The and or trick is useful in lambda functions where if expressions are not allowed.
