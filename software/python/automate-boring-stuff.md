## Automate the boring stuff

- Download extra from <https://nostarch.com/automatestuff2/>
- Uses openpyxl 2.6.2 in the book
	- to install:  `pip install --user -U openpyxl==2.6.2`
- Find cwd: 
	- import `os`, then `os.getcwd()`, change it with `os.chdir()wb`

### openpyxl commands
- `wb = openpyxl.load_workbook('example.xlsx')`
- `wb.sheetnames # The workbook's sheets' names.`
- `sheet = wb['Sheet3'] # Get a sheet from the workbook.`
- `sheet.title # Get the sheet's title as a string.`
- `anotherSheet = wb.active # Get the active sheet. This is the top sheet when you open Excel`
- `sheet['A1'] # Get a cell from the sheet.`
- `sheet['A1'].value # Get the value from the cell. Cell objects have row, column, coordinate attributes`
- `sheet.cell(row=1, column=2) # return coordinate since Excel letters hard to get programatically`

```python
>>> sheet.cell(row=1, column=2)  
<Cell 'Sheet1'.B1>  
>>> sheet.cell(row=1, column=2).value  
'Apples'  
>>> for i in range(1, 8, 2): # Go through every other row:  
...     print(i, sheet.cell(row=i, column=2).value)
```

- `sheet.max_column # Get the highest column number.`
- switching between column numbers and letters
	- `from openpyxl.utils import get_column_letter, column_index_from_string`
	- `get_column_letter(sheet.max_column)
	- `get_column_letter(27)`
	- ***This one is useful!*** `column_index_from_string('AA') #returns 27`
 - Get rows and columns from sheets
	 - ` tuple(sheet['A1':'C3']) # Get all cells from A1 to C3.`

```python
## Get the rectangular area from A1 to C3

>>> for rowOfCellObjects in sheet['A1':'C3']:  
...     for cellObj in rowOfCellObjects:  
...         print(cellObj.coordinate, cellObj.value)  
...     print('--- END OF ROW ---')

```
