## DDD summarized

- Uses bounded context because modeling the entire system would be too complex.
- DDD uses a [ubiquitous language](https://martinfowler.com/bliki/UbiquitousLanguage.html)
- CQRS with event sourcing came later and has contributed to DDD

## Monty Python quote

In a TV talk show interview, comedian John Cleese told a story of an event during the filming of Monty Python and the Holy Grail. They had been shooting a particular scene over and over, but somehow it wasn’t funny. Finally, he took a break and consulted with fellow comedian Michael Palin (the other actor in the scene), and they came up with a slight variation. They shot one more take, and it turned out funny, so they called it a day.

The next morning, Cleese was looking at the rough cut the film editor had put together of the previous day’s work. Coming to the scene they had struggled with, Cleese found that it wasn’t funny; one of the earlier takes had been used.

He asked the film editor why he hadn’t used the last take, as directed. “Couldn’t use it. Someone walked in-shot,” the editor replied. Cleese watched the scene again, and then again. Still he could see nothing wrong. Finally, the editor stopped the film and pointed out a coat sleeve that was visible for a moment at the edge of the picture.


## INGREDIENTS OF EFFECTIVE MODELING

Certain things we did led to the success I just described.

1. Binding the model and the implementation. That crude prototype forged the essential link early, and it was maintained through all subsequent iterations.

2. Cultivating a language based on the model. At first, the engineers had to explain elementary PCB issues to me, and I had to explain what a class diagram meant. But as the project proceeded, any of us could take terms straight out of the model, organize them into sentences consistent with the structure of the model, and be unambiguously understood without translation.

3. Developing a knowledge-rich model. The objects had behavior and enforced rules. The model wasn’t just a data schema; it was integral to solving a complex problem. It captured knowledge of various kinds.

4. Distilling the model. Important concepts were added to the model as it became more complete, but equally important, concepts were dropped when they didn’t prove useful or central. When an unneeded concept was tied to one that was needed, a new model was found that distinguished the essential concept so that the other could be dropped.

5. Brainstorming and experimenting. The language, combined with sketches and a brainstorming attitude, turned our discussions into laboratories of the model, in which hundreds of experimental variations could be exercised, tried, and judged. As the team went through scenarios, the spoken expressions themselves provided a quick viability test of a proposed model, as the ear could quickly detect either the clarity and ease or the awkwardness of expression.

---

DDD models bounded context. The entire system may be too complex to model. 

> Highly productive teams grow their knowledge consciously, practicing continuous learning (Kerievsky 2003). For developers, this means improving technical knowledge, along with general domain-modeling skills (such as those in this book). But it also includes serious learning about the specific domain they are working in

## Resources

- My [notes](./domain-driven-design-evans.md) from the Evans book
- [Interview](https://www.youtube.com/watch?v=GogQor9WG-c) with Eric Evans
- DDD, tests, BDD [talk](https://www.youtube.com/watch?v=xSZnKnZ8EAo) with Kenny Baas
- <https://cqrs.nu/Faq>
- <https://martinfowler.com/bliki/BoundedContext.html>
- <http://codebetter.com/gregyoung/2010/02/16/cqrs-task-based-uis-event-sourcing-agh/>


## TODO
  
- [ ] Look up how CQRS and event sourcing has changed DDD
- [ ] Look at Fowler blog on CQRS with event sources
- [ ] See if Visual Studio has class diagramming tools
