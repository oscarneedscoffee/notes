# Domain Driven Design 

By Eric Evans

## Part 1 Putting the Domain Model to Work

**Knowledge Crunching**: Effective modelers are knowledge crunchers.  It's not a solitary activity. Devs and domain experts collaborate.

### Making sense of domain design

A domain is the subject of interest for the user

A model is a set of tools to help understand the domain. 
-knowledge crunching

### Making use of language

Domain experts use their own language. Developers use their own too. Sometimes some are bilingual but they become bottlenecks. So devs have to translate for domain experts, experts to devs, and experts to other experts. This translation muddles concepts and may lead to destructive refactoring. 

>Translation blunts communication and makes knowledge crunching anemic.

The overhead of translation is too high, so teams need a common language. The domain model provides the backbone for that common language. 

See: The Language Instinct, by Steven Pinker [Pinker 1994]).

### INGREDIENTS OF EFFECTIVE MODELING

Certain things we did led to the success I just described.

1. Binding the model and the implementation. That crude prototype forged the essential link early, and it was maintained through all subsequent iterations.

2. Cultivating a language based on the model. At first, the engineers had to explain elementary PCB issues to me, and I had to explain what a class diagram meant. But as the project proceeded, any of us could take terms straight out of the model, organize them into sentences consistent with the structure of the model, and be unambiguously understood without translation.

3. Developing a knowledge-rich model. The objects had behavior and enforced rules. The model wasn’t just a data schema; it was integral to solving a complex problem. It captured knowledge of various kinds.

4. Distilling the model. Important concepts were added to the model as it became more complete, but equally important, concepts were dropped when they didn’t prove useful or central. When an unneeded concept was tied to one that was needed, a new model was found that distinguished the essential concept so that the other could be dropped.

5. Brainstorming and experimenting. The language, combined with sketches and a brainstorming attitude, turned our discussions into laboratories of the model, in which hundreds of experimental variations could be exercised, tried, and judged. As the team went through scenarios, the spoken expressions themselves provided a quick viability test of a proposed model, as the ear could quickly detect either the clarity and ease or the awkwardness of expression.


---

DDD models bounded context. The entire system may be too complex to model. 

> Highly productive teams grow their knowledge consciously, practicing continuous learning (Kerievsky 2003). For developers, this means improving technical knowledge, along with general domain-modeling skills (such as those in this book). But it also includes serious learning about the specific domain they are working in

## TODO

- [ ]  Look up event sourcing and message queuing. Evans mentions hoe important it changed DDD in an interview
- [ ]  See if Visual Studio has class diagramming tools

## resources

Kerievsky, J. 2003. “Continuous Learning,” in Extreme Programming Perspectives, Michele Marchesi et al. Addison-Wesley.

Kerievsky, J. 2003. Web site: http://www.industriallogic.com/xp/refactoring