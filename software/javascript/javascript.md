# Javascript notes

General notes from all over the place.
<!-- TOC -->

- [Javascript notes](#javascript-notes)
    - [Brain-dump](#braindump)
    - [ECMAScript 2017 Specification](#ecmascript-2017-specification)
    - [JS: The Good Parts](#js-the-good-parts)
    - [Node JS](#node-js)
    - [Angular JS Book](#angular-js-book)
    - [Resources](#resources)
        - [JS Standard](#js-standard)
        - [Useful Links](#useful-links)
        - [Tools](#tools)
        - [Online Editors and Sandboxes](#online-editors-and-sandboxes)
        - [Source](#source)
        - [Read-See More](#read-see-more)

<!-- /TOC -->

## Braindump


## __this__ in Javascript

The way __this__ is bound to an object depends on the way a function is invoked. The four ways to invoke a function are:

1. As a method
```javascript
var obj = {};
obj.mymethod = function(){ 
// myMethod is a Method. 
console.log(this); 
// this is bound to obj
};
```

2. As a function
```javascript
function fun(){
    // fun is a function.
   console.log(this);
  // this is bound to global object which is "window" object in case of browsers for the front end JavaScript.
}
 
fun(); // function fun is invoked.
```

3. As a constructor
```javascript
function Fun(){
// Notice a function being used as a constructor always starts with Capital letter.
    this.myProperty = ' This is my property';
}
var myObject = new Fun();
```

4. With Apply
```javascript
function foo(a,b){
    console.log(a,b,this);
}
var args = ['abra','ka', 'dabra'];
foo.apply(obj,args);
```
the above examples of __this__ came from here:  
<https://www.quora.com/What-does-this-mean-in-Javascript/answer/Manish-Dipankar-1?share=1&srid=X7KE>

Another article on [__this__](https://codeburst.io/javascript-the-keyword-this-for-beginners-fb5238d99f85)


<!----------------- ES 2017 ------------------------------------>
## ECMAScript 2017 Specification

Citation [2017 specification](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf)

The primitive builtin types are 
1. Undefined
1. Null
4. Boolean
4. Number


<!-- -------------- JS Good parts ----------------------- -->
## JS: The Good Parts

>I stopped writing my own notes for this books because of laziness, but here are some notes I found on [Github](https://github.com/dwyl/Javascript-the-Good-Parts-notes)
> [Source from book here](https://resources.oreilly.com/examples/9780596517748/)


by Douglas Crockford

A controversial feature in JavaScript is prototypal inheritance. JavaScript has a class-free object system in which objects inherit properties directly from other objects. This is really powerful, but it is unfamiliar to classically trained programmers.

If you attempt to apply classical design patterns directly to JavaScript, you will be frustrated. But if you learn to work with JavaScript's prototypal nature, your efforts will be rewarded. **(Crockford, 1.2)**

One bad part of JS is global variables for linkage.  All top-level variables are in a global namespace called **global object.**
  
Block comments will not work for regular expression literals, so it’s best to use line comments exclusively 

The value NaN is a number value that is the result of an operation that cannot produce a normal result. NaN is not equal to any value, including itself. You can detect NaN with the isNaN( number ) function.

<https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach?redirectlocale=en-US&redirectslug=JavaScript%2FReference%2FGlobal_Objects%2FArray%2FforEach>

> “It is your duty to use available prior art before attempting to roll your own code. Concentrate on the 15% that makes your application unique, and “offshore” the grunt work to well-established third-party libraries.
Equally important as reusing other people’s code is reusing your own code.” (Trostler, Reuse ch 2)



<!-- ----------- Node.JS -------------------- -->
## Node JS

[Introduction to Node with Ryan Dahl 2011](http://www.youtube.com/watch?v=jo_B4LTHi3I)


Node beginner

* <http://www.nodebeginner.org/>
* <http://nodeschool.io>
* <http://howtonode.org/>
* <http://docs.nodejitsu.com/>

[Node dependency check](http://stackoverflow.com/questions/16073603/how-do-i-update-each-dependency-in-package-json-to-the-latest-version)



<!-- ------------ Angular JS ---------------------- -->
## Angular JS Book

by Chris Smith

<http://www.angularjsbook.com>

[Angular tutorial](https://scotch.io/tutorials)


<!-- ----------------------------- RESOURCES ------------------------>
## Resources

My JS test projects on [Github](https://github.com/ocpineda/js-tutorial)

### JS Standard

* [Javascript Standard](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf)
* [ES 5](http://es5.github.io)

### Useful Links

* [Javascript: The Good Parts](http://www.youtube.com/watch?v=hQVTIJBZook)
* [Eloquent Javascript](http://eloquentjavascript.net/)
* [All things JS from MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript?redirectlocale=en-US&redirectslug=JavaScript)
* [What D3 is not](http://ruoyusun.com/2014/05/26/what-d3js-is-not.html)
* [JS Modules](http://exploringjs.com/es6/ch_modules.html)

### Tools

* JSLint:  a js parser which will report on the bad parts
* [jsmeter: measures cyclomatic complexity](http://code.google.com/p/jsmeter/)
* [jscheckstyle: cyclomatic complexity](https://github.com/nomiddlename/jscheckstyle)
* [Kong API](http://getkong.org)

### Online Editors and Sandboxes
- [JS Bin](https://jsbin.com)
- [CodePen](https://codepen.io)
- [Code Sandox](http://codesandbox.io)
- [Repl.it](https://repl.it)
- [JS Fiddle](https://jsfiddle.net)

### Source

[Testable Javascript:](https://github.com/zzo/TestableJS)

### Read-See More

- [A Complexity Measure by Thomas McCabe](http://www.literateprogramming.com/mccabe.pdf)
- [Explanation of non-blocking in node.js](http://www.youtube.com/watch?v=lzS2Tkxu9y4)
- [Survive JS tutorial](https://survivejs.com/maintenance/introduction/)
- [Shotgun with Eric Elliot](https://ericelliottjs.com/)
