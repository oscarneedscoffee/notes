# React Definitions

## Element
A react element is the smallest building blocks of React. Elements can have properties (props), and __components__ are made up of __elements__.   
  
Elements are immutable
[read more](https://reactjs.org/docs/rendering-elements.html)

## Redux
A state management tool for JS. More [here](redux.md)


