# React & React Native 2nd edition
By Adam Boduch

## brain dump

React is the view layer.  

<img src="react-layers.png" width="101" height="176">

---
## React APIs
React has two main APIs, the __React DOM__, and the __React Component__
<img src="react-apis.png" width="300" > 

### React Component API
- Data
- Lifecycle: 
  where we implement lifecycle changes. e.g. components about to be renderd
- Events: code we write for user interactions
- JSX

---

## Server Side Rendering
Isomorphic is another name for server side rendering.
>This is a fancy way of saying JavaScript code that can run in the browser and in Node.js without modification

Server Side Rendering (SSR) is faster. *The initial load time seems faster to the user*.   

Why is it faster?
- Instead of rendering JS in the browser, the rendering is a string and done on the server. The string is then sent to the browser.
- There are fewere network requests to fetch from the API.

---
## Glossary 
- JSX
- Server Side Rendering SSR


## Resources
- [React Github](https://github.com/facebook/react)
- [Source for this book](https://github.com/PacktPublishing/React-and-React-Native-Second-Edition)

