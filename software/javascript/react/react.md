## Braindump

## JSX: 
prevents XSS since everything is converted to string before rendering in `ReactDom.render()`  

## Components & Props

[read more](https://reactjs.org/docs/components-and-props.html)

## Tooling

- [Gatsby static site generator](https://www.gatsbyjs.org)
- [Material UI is a React implementation of Google's Material Design](https://material-ui.com/)

## Glossary

[definitions]:react-definitions.md

- [element](react-definitions.md#element)
- component
- JSX
- props
- [Redux](react-definitions.md#redux)
- state

## Resources

- Server side rendering info [Demystifying React SSR](https://medium.freecodecamp.org/demystifying-reacts-server-side-render-de335d408fe4)
- Modern React Bootcamp Udemy [course by Colt Steele](https://www.udemy.com/course/modern-react-bootcamp) and my [notes](udemy-modern-react-bootcamp/react-bootcamp.md)
    

## Books
- [React & React UI by Adam Boduch](react-and-react-native.md)
- [JS the Right Way](https://jstherightway.org/)
- [Essential React Libraries and Frameworkes](https://www.robinwieruch.de/essential-react-libraries-framework/) 

