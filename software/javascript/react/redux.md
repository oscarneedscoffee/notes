# Redux

Redux is a state management tool. It's used in React, but can be used by any JS framework.

## Resources

- [A Cartoon Guide](https://code-cartoons.com/a-cartoon-intro-to-redux-3afb775501a6)
- [Redux tutorial](https://www.valentinog.com/blog/redux/)
- [Why use Redux?](https://blog.logrocket.com/why-use-redux-reasons-with-clear-examples-d21bffd5835/)
