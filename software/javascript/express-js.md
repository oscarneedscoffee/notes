## Parts
- middleware
- routing
- sub-applications
- conveniences

## Installing
<https://expressjs.com/en/starter/installing.html>  

To create an application skeleton: <https://expressjs.com/en/starter/generator.html>

## Running
After you install the skeleton, run `npm start` and go to `http://localhost:3000`

---

## Static Files
`app.use(express.static('public'))`  

To create a virtual path, specify a mount path:  
`app.use('/static', express.static('public'))`  

The path provided is relative. It's better to give the full path:  
`app.use('/static', express.static(path.join(__dirname, 'public')))`

--

## Response methods

| Method          | Description                     |
| --------------- |:-------------------------------:|
| res.download()  | Prompt a file to be downloaded. |
| res.end()       | End the response process. |
|res.json()	      | Send a JSON response. |
|res.jsonp()      | 	Send a JSON response with JSONP support. |
|res.redirect()	  | Redirect a request. |
|res.render()	    | Render a view template. |
|res.send()       | Send a response of various types. |
|res.sendFile()	  | Send a file as an octet stream. |
|res.sendStatus() | Set the response status code and send its string representation as the response body. |

## Routing
A *Router* is a definition of application end points and how they respond to clients.

The guide is [here](https://expressjs.com/en/guide/routing.html).  
You can use basic routing  with the *route* method which is part of the `express` class.  
You can also route with express.Router() class. This creates mountable route handlers.  

So why use the middleware? This answer on [StackOverflow may help](http://stackoverflow.com/questions/28305120/differences-between-express-router-and-app-get)  

## Middleware  
- [Guide](https://expressjs.com/en/guide/writing-middleware.html)  
- To load a middleware function, call `app.use()`  
- The order of middleware functions matter. FCFS order.

Middleware functions take 3 functions parameters, req, res, and next `function (req, res, next)`  The next parameter is a callback to the middleware function.
