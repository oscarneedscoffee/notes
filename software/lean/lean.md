- [Lean Principles](#lean-principles)
- [PDCA](#pdca)
- [Process Thinking](#process-thinking)
  - [The Value Stream](#the-value-stream)
  - [Going to the source](#going-to-the-source)
- [The 8 Forms of Waste DOWNTIME](#the-8-forms-of-waste-downtime)
- [Process Maps](#process-maps)
  - [Map types](#map-types)
  - [The Pace of Customer Demand](#the-pace-of-customer-demand)
  - [Spaghetti Diagram](#spaghetti-diagram)
- [5 Whys, 1-How Analysis](#5-whys-1-how-analysis)
- [5-S Approach to Workplace Organization](#5-s-approach-to-workplace-organization)
- [Visual Management](#visual-management)
- [Documenting Standard Work](#documenting-standard-work)
  - [The 4 Key elements of standard work sheet instructions](#the-4-key-elements-of-standard-work-sheet-instructions)
  - [Additional standard work resources](#additional-standard-work-resources)
- [Leader Standard Work Toolset](#leader-standard-work-toolset)
  - [Some best practices for leader standard work](#some-best-practices-for-leader-standard-work)


## Lean Principles
1. ID customers and specify value
2. Map the value stream
3. Create flow
   1. Quick changeover
   2. Line balancing: flow of work through processes is nearly continuous
   3. Cellular processing: allows for small batch sizes
4. Respond to pull
   1. Kanban
   2. JIT
   3. Demand base scheduling
5. Pursue perfection
   1. Kaizen
   2. Total Productive Maintenance (TPM): continuous improvement
   3. Standardized work: systematic process documentation
   4. Error proofing: the absence of errors

## PDCA

Plan-Do-Check-Act loop
- **Plan**
- **Do**: 
  - actions for improvements are tested on small scale, results documented
- **Check**
  - after Do, data is studied and results documented. Failures are analyzed and lessons are documented
- **Act**
  - Effective actions are adopted and documented as new standard work. *Ineffective* goes back to the plan stage. The cycle continues.

## Process Thinking
- Value stream. 
- The process input is `Y=f(X)`. Everything is a process

### The Value Stream

A **value stream* are processes that include steps where customer defined value is created.
The value stream is represented by the **Supplier-Input-Processing-Output-Customer** chain (SIPOC)

### Going to the source

Go to the source of where work is performed to learn by observing. It's called **Gemba**
> "Go see, ask why, show respect"

- [ ] look up Gemba walks

## The 8 Forms of Waste DOWNTIME 

DOWNTIME is a mnemonic to describe forms of waste

- **Defects:** 
  - failure to meet the requirements of internal or external customers.
- **Overproduction**: 
  - producing at a rate that exceeds demand.
- **Waiting**: 
  - time spent in queues between steps in a process.
- **Non-utilized Resources**
  - people are seen as a source of labor only and are told what to do and not to think; they are not consulted for improvement ideas.
- **Transportation**
  - movement of materials or information from one place to another. Movement does not create value, and each handling step entails a potential for error.
- **Inventory**
  - inventory held in excess of the quantity demanded. Nothing good happens to excess inventory: it gets damaged, lost, stolen, or becomes obsolete. Even worse, it hides quality and production problems.
- **Motion**
  - inefficient workstation design requiring excess bending, walking, reaching, handling, lifting, or awkward grasping. Activity does not equal work!
- **Excessive Processing**
  - using the wrong process, including over-automating or under-automating; inspection that does not take place at the source of potential defects. Adding more than the customer is willing to pay for.

## Process Maps

Process maps capture process knowledge in a visual format that facilitates easy communication.

### Map types
- SIPOC
  - Suppliers, Inputs, Outputs, Customers
- Flow Chart
- Swimlane Chart
- Value Added Flow Chart
  - used to highlight waste.
  - Less than 10% of total processing time actually creates value.
- Spaghetti Diagram
  - shows physical flow
- Value Stream Map
  - nonlinear, and examines process behavior

### The Pace of Customer Demand
- Production must be just right. Too much and you waste time, too little and you don't meet customer demands.
- The pace of customer demand is called `takt time` from "taktzeit" in German.
  - `Takt time = total available process time / total quantity required by customer`
- A tool to compare processing time is a **Takt Time Chart**

### Spaghetti Diagram
- Two types, one shows the *physical movement of material*, and the other shows the *movement of information*
- Identify non-value added operations for information spaghetti, or excessive motion for physical spaghetti.

## 5 Whys, 1-How Analysis  
- Related to [Fishbone diagram](https://en.wikipedia.org/wiki/Ishikawa_diagram)
- Ask "why?" a bunch of times, then ask how. The how is the permanent corrective action after identifying the root cause.

## 5-S Approach to Workplace Organization

This approach claims that tidyness correlates to effectiveness.

- Sort
- Set in Order
- Shine
- Standardize
- Sustain

## Visual Management

Keep everyone informed and involved in the organizations performance. There are three elements required:

1. Charts, graphs, and displays of performance metrics.
   1. Examples are dashboards, huddle boards, story boards, and andon cords
2. Removal of visual obstructions
   1. Piles of inventory hides problem. Messy (see 5-S).
   2. Reducing rack height forces you to reduce inventory
   3. Simple visual signals can be used when operations are continuous and contiguous. Simple communication
3. Workstation cleanliness and organization (see 5-S).

## Documenting Standard Work
- **Standard Work** is a term coined by Toyota to represent the established best way of performing the work within a process. 

### The 4 Key elements of standard work sheet instructions
1. Major Steps
2. Key Points: key for performing a step
3. Reason Why
4. Visual Reference

### Additional standard work resources
- [NPR article](http://www.npr.org/templates/story/story.php?storyId=122226184)
- [Checklist Manifesto guy](https://www.newyorker.com/magazine/2007/12/10/the-checklist)

## Leader Standard Work Toolset

- Lean needs support from the top.
- Standardized leader work is a series of habits. Hints at [Duhigg book](https://charlesduhigg.com/the-power-of-habit/)

The four elements of lean management are. 
- Leader Standard work:
  - What, when, by whom?
- Visual Management
- Daily Accountability:
  - Did we do what we planned?
- Gemba Visits

Standard work establishes a culture of continuous improvement – a pattern of habits of looking for better ways to do things while doing things the known best way. [Read more about it](https://www.mckinsey.com/business-functions/operations/our-insights/continuous-improvement-make-good-management-every-leaders-daily-habit)


### Some best practices for leader standard work

| ELEMENT           | INTENT                      |
|-------------------|:----------------------------|
|Standard Work Plan | Written plan to encourage discipline & consistency, adjusted based on learning.|
| Gemba Walks | Go to actual place, observe the actual process, talk to the actual people. |
| Visual Management | Make problems visible. Make metrics and work plans present and visible to all. |
| Stand-up Meetings | Efficient team communication, coordination, and problem-solving. |
| 1:1  Coaching     | Develop people through question-driven coaching. |
| System of Accountability for Actions | All must understand who is committed to do what by when. |
| Kanban for Scheduled Work  | Prioritize work and make the plan visible. |