# HTTP Developers Handbook 
*by Chris Shiftlett*

## References

- <http://www.w3.org.History.html>
- <http://www.google.com/archive_announce_20.html>
- <http://www.w3.org/draft-lafon-rfc2616bis-03.html>


## Networking Protocols
   
from bottom to top:  
- Network
- Internet (IP)
- Transport (TCP)
- Application Layer (HTTP)

URLs <http://www.w3.org/Addressing/>  
HTML http://www.w3c.org/MarkUp/


## Http Transactions 

Connections, Requests, Responses:    
A connection refers to a TCP connection of tcp syn, tcp syn + ack, and tcp ack

RSS Specification:  
The definition is currently at <http://my.netscape.com/publish/formats/rss-0.9.1.dtd>

The latest version of RSS  <http://purl.org—spec>
\*See Chapter 21\* about intelligent architecture.   If you redirect your users to a
    different URL just to display an error message, you have a failed design.

<!-- ***********  HTTP REQUESTS  ************** -->
## HTTP Request

Three Parts: 
1. Request line
1. HTTP headers
1. Content

An example of an HTTP request

```
GET /search?hl=en&q=HTTP&btnG=Google+Search HTTP/1.1 
      Host: www.google.com 
      User-Agent: Mozilla/5.0 Galeon/1.2.0 (X11; Linux i686; U;) Gecko/20020326 
      Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9, 
              text/plain;q=0.8, video/x-mng,image/png,image/jpeg,image/gif;q=0.2, 
              text/css,*/*;q=0.1 
      Accept-Language: en 
      Accept-Encoding: gzip, deflate, compress;q=0.9 
      Accept-Charset: ISO-8859-1, utf-8;q=0.66, *;q=0.66 
      Keep-Alive: 300 
      Connection: keep-alive
```

You can go into telnet and view headers. To view GET for google.com type:  
      
`telnet www.google.com 80`
      
Once you are in the telnet prompt type 
`GET / HTTP/1.x`  

You use the same syntax for the other request methods

<!-- ***********   REQUEST Methods ************** -->
## Request Methods

> GET, POST, PUT, DELETE, HEAD, TRACE, OPTIONS, and CONNECT


### The GET Method

 - allows users to bookmark requests.
 - Usually contains no content and thus no entity headers

### The POST method
- good for sending user data that shouldn't be shown in the url string.
- Apache can handle up to 1024 bytes in the query string and recommends no more than 255 characters.
- POST will contain entity headers

### The PUT method
- Allows the web client to send content that will be stored on the web server.
- The semantics are similar to POST except the resource in the request line is the requested location is the content to be stored instead of the resource intended to receive the content.  Put will respond with 201 Created if the content is created or 204 No Content
- PUT is rarely implemented in Web clients.  <form> only accepts the value of get or post

### The DELETE method
If successful it will return 200 OK, but it does not necessarily mean the content is deleted.

### The HEAD method
- A very useful method.  HEAD behaves exactly like GET except you don't get the content.
- HEAD is not perfectly dependable because the resource generating the response takes the request method into consideration.


### The TRACE method

A diagnostic request method.  Follows any proxies.


### The OPTIONS method
> OPTIONS * HTTP/1.1  
> Host: 127.0.0.1

If you specify a specific resource OPTIONS will show much more


### The CONNECT method

- Reserved explicitly for use by intermediary servers to create a tunnel to the destination server.  The intermediary, not the HTTP client, issues the CONNECT request to destination.
- Unlike a proxy a tunnel does not interpret the HTTP requests and it does not cache traffic. The most common use of CONNECT is SSL or TLS
- CONNECT is also used for an SSL accelerator.

<!-- ***********  REQUEST HEADERS  ************** -->
## Request Headers

    ACCEPT, Accept-Charset, Accept-Encoding, Accept-Language, Authorization, Cookie, Expect, From, Host

    If-Match, If-Modified, If-None-Match, If-Range, If-Unmodified, Max-Forwards, Proxy-Authorization, Range,Referer, TE, User-Agent

### The ACCEPT header:
Displays types of content that is accepted and in what order.  This is not the same as allowing a browser to natively support a specific type.

### The Host header
allows for multihoming: Multiple hosts served by a single IP.

<!-- ***********  STATUS CODES  ************** -->

## HTTP Status Codes

<http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html>

### 1xx  informational
    100 continue
    101 switching protocols

### 2xx Successful
    200 OK
    201 Created
    202 Accepted
    203 Non-Authoritative Information
    204 No Content
    205 Reset Content
    206 Partial Content

### 3xx Redirection
    300 Multiple Choices
    301 Moved Permanently
    302 Found
    303 See Other
    304 Not Modified
    305 Use Proxy
    306 Unused
    307 Temporary Redirect
      
### 4xx Client Error
    400 Bad Request
    401 Unauthorized
    402 Payment Required (reserved for future use)
    403 Forbidden
    404 Not Found
    405 Method Not Allowed
    406 Not Acceptable
    407 Proxy Authentication Required
    408 Request Timeout
    409 Conflict
    410 Gone
    411 Length Required
    412 Precondition Failed
    413 Request Entity Too Large
    414 Request-URI Too Long
    415 Unsupported Media Type
    416 Requested Range Not Satisfiable
    417 Expectation Failed 

### 5xx Server Error
    500 Internal Server Error
    501 Not Implemented
    502 Bad Gateway
    503 Server Unavailable
    504 Gateway Timeout
    505 HTTP Version Not Supported


