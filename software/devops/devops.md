
## Velocity 09 notes
An automated infrastructure consists of  role & config management, and OS imaging

### Tools
- automated infrastructure
- shared version control
- One step build

---

## Phoenix Project

by Gene Kim, Kevin Behr, & George Spafford (2013)

### Resources that inspired the book

- The Theory of Constraints by Eliyahu Goldratt
- The Goal by Goldratt
- The Art of Capacity Planning by John Allspaw
- Continuous Delivery by Jez Humble & David Farley
- Risk Adjusted Value Management by Paul Proctor & Michael Smith
- GAIT IT Controls by  Institute of Internal Auditors
- 2013 State of Dev Ops by Puppet


---

## Resources
- The Phoenix Project
- The Devops Handbook
- [Velocity '09 conference Allspaw & Hammond](https://www.youtube.com/watch?v=LdOe18KhtT4)
