# Notes from Continous Delivery by Farley & Humble

## Problems with Delivering Software Ch 1.
Central to understanding CD is the concept of a *deployment pipeline*. That is an automated implementation of the build, deploy, test, and release process.

- Deployments should lean towards being fully automated; pick a version and press a button

### Antipattern: Deploying to a Production-like Environment Only after Development Is Complete

Doing this will create stage, and production errors. The environments will also diverge. There won't be enough testing.

To fix these problems, integrate testing, development, and release activites into the dev process.

### Antipattern: Manually configuring a change in a production environment

Some traits of this antipattern are:

- After testing stage, production has bugs
- Different nodes in a cluster behave differently
- Long time to release to prod.
- You can't rollback to an earlier config

Instead you  should apply configuration through version control through an automated process.

Every single piece of the infrastructure should be re-creatable. This includes the OS, application stack, configurations, etc... *Virtualization* can help with this.

This book aims to find ways to reduce **Cycle Time**, the time it takes from deciding to make a change to haveing it available to users.

Software should fit its purpose, so quality does not equal perfection, but the goal is to deliver software of sufficient quality.

Goals:

- Low cycle time
- High quality

To achieve the above goals our releases should be *frequent*, and *automated*.

***Automated.** If build, deploy, and test is not automated it is not repeatable. The software is different for every release, and a manual process will be error prone. 

>Releasing software should be an engineering discipline, not an art.

**Frequent.** If releases are frequent, the changes between them will be small which reduces risk. High frequency also encourages feedback. **Feedback is essential to frequent, automated releases.**

Three criteria to useful feedback:

1. Any change at all needs to trigger the feedback process
2. Feedback must be deliveres ASAP
3. The delivery team must receive the feedback and act on it.

### Every Change Should Trigger the Feedback Process

Any change in *executable code, configuration, environment, or data* can lead to a change in the application's behavior.

Testing on every build and checkin is known as **continous integration**  