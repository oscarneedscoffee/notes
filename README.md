# Oscar's Notes

I have notes all over the place on all sorts of mediums. Since git is part of my regular workflow and a significant amount of my notes are in disparate chunks online, I decided to try to ~~keep~~ reference them all in gitlab. This is still a work in progress.

More bookmarks [here](https://github.com/ocpineda/my-lists)

# Table of Contents

- [Oscar's Notes](#oscars-notes)
- [Table of Contents](#table-of-contents)
- [API Design](#api-design)
- [C](#c)
- [Cloud](#cloud)
- [Computer Science](#computer-science)
- [Cyber Security](#cyber-security)
- [Design Patterns](#design-patterns)
- [Devops](#devops)
- [Eiffel](#eiffel)
- [Go](#go)
- [History](#history)
- [HTTP](#http)
- [Javascript](#javascript)
- [Kubernetes](#kubernetes)
- [Lean](#lean)
- [Microservices](#microservices)
- [Networking Computers](#networking-computers)
- [Personal Finance](#personal-finance)
- [PHP](#php)
- [Productivity](#productivity)
- [Python](#python)
- [React](#react)
- [Refactoring](#refactoring)
- [Self Improvement](#self-improvement)
- [Software Development](#software-development)
- [Studying Tips](#studying-tips)
- [Web Development](#web-development)

# API Design
- [API Design notes](software/api/api.md)

# C#

- [C# Notes](software/c-sharp/c-sharp.md)
- [Entity framework notes](software/c-sharp/entity-framework.md)
- [Pro C# 7 book notes](software/c-sharp/booknotes/pro-c-sharp-7.md)

# Cloud
- [Azure](software/azure/azure.md)


# Computer Science
- [CS 4501: my notes](software/cs4501-programming-languages/csci%204501%20lecture%20notes.pdf)

# Cyber Security
- [Attack Surface Management]()
- [CISSP](software/security/cissp/cissp.md)
- [CompTIA Security+ Exam](software/security/security-plus/security-plus-exam.md)
- [Cyber Security notes](software/security/security.md)
- [Cyber Security resources](software/security/security-resources.md)
- [Foundations of Information Security](software/security/foundations-info-sec.md)

# Design Patterns

- [Repository Pattern in PHP](software/design-patterns/repository-pattern.md)
- [Inversion of Control & dependency injection](software/design-patterns/inversion-of-control.md)

# Devops

- [Continuous Delivery](software/devops/continous-delivery.md)
- [Devops notes](software/devops/devops.md)

# Eiffel

- [Eiffel Object Oriented Software Construction](software/eiffel.md)

# Go

- [Go lang notes](software/golang/golang.md)

# History

- [D-Day: June 6, 1944:](history/d-day-ambrose.md)

# HTTP

- [HTTP Developer's Handbook](software/http-developers-handbook.md)

# Javascript

- [Express JS](software/javascript/express-js.md)
- [JS Notes](software/javascript/javascript.md)
- [Vue JS](software/javascript/vue-js.md)

# Kubernetes
- [Kubernetes notes](software/kubernetes/kubernetes.md)

# Lean
- [Lean notes](software/lean/lean.md)

# Microservices

- [Microservices notes](software/microservices/microservice-readme.md)
- [Production Ready Microservices](software/microservices/production-ready-microservices.md)

# Networking Computers
- [Networking notes](networking-computers/networking.md)

# Personal Finance

- [Investing](personal-finance/investing-101.md)
- [Pound Foolish](personal-finance/pound-foolish/pound-foolish.md)

# PHP

- [Essential PHP Security](software/PHP/essential-PHP-security/essential-PHP-security.md)
- [Modernizing Legacy PHP](software/PHP/modernizing-legacy-php.md)
- [PHP general notes](software/PHP/PHP-general-notes/php-general-notes.md)

# Productivity

- [Atomic Habits](productivity/atomic-habits.md)
- [Digital Minimalism by Cal Newport](productivity/digital-minimalism.md)
- [List of relaxing videos and music](productivity/videos-music-playlists.md)
- [Laser Sharp Focus](productivity/laser-sharp-focus.md)
- [Ultralearning by Scott H. Young](productivity/Ultralearning.md)


# Python

- [Python tutorial](software/python/python.md)

# React
- [React](software/javascript/react/react.md)
- [Gatsby](software/javascript/react/Gatsby.md)
- [Udemy Modern React Bootcamp course](software/javascript/react/udemy-modern-react-bootcamp/react-bootcamp.md)

# Refactoring

- [Sourcemaking tutorial](software/refactoring/sourcemaking-tutorial.md)

# Self Improvement

- [The Book of Five Rings](http://opineda.com/five-rings.html)
- [Educational Mithradatism (blog post by Zed Shaw)](https://zedshaw.com/2015/09/14/educational-mithridatism/)
- [Grit by Angela Duckworth](self-help/grit-duckworth.md)
- [Maximum Energy for Life](self-help/maximum-energy-for-life.md)
- [Remember It by Nelson Dellis](self-help/remember-it-dellis.md)
- [Thinking Fast & Slow](self-help/thinking-fast-and-slow.md)
- [Unleash the Warrior Within](self-help/unleash.md)
- [Your Memory and How it Works](self-help/your-memory-how-it-works.md)


# Software Development

- [Cheatsheets](https://github.com/ocpineda/cheat-sheets)
- [Code Reading (Spinellis 2003)](software/code-reading.md)
- [Domain Driven Design](software/domain-driven-design/domain-driven-design.md)
- [Working Effectively with Legacy Code (Feathers 2004)](software/working-effective-with-legacy-code.md)
- [Test Driven Development (Kent Beck)](software/test-driven-development.md)

# Studying Tips

- [Coursera notes: Learning How to Learn](studying/coursera-how-to-learn/coursera-how-to-learn.md)
- [How to Use Your Mind to Study](studying/kitson/how-to-use-your-mind.md) by _Harry Kitson_
- [A Mind for Numbers by Barbara Oakley](studying/mind-for-numbers.md)
- [Studying Tips tutorials](studying/studying-tips)

# Web Development

- [Responsive Design](web-dev/responsive-design.md)

