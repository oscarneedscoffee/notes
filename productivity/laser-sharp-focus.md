# Laser Sharp Focus

*by Joanna Jast*

<!-- TOC -->

- [Laser Sharp Focus](#laser-sharp-focus)
    - [CH1 THREE COMPONENTS OF FOCUS](#ch1-three-components-of-focus)
        - [2 types of focus problems](#2-types-of-focus-problems)
    - [CH 2 Focus Danger Zones](#ch-2-focus-danger-zones)
    - [CH3 GOAL SETTING](#ch3-goal-setting)
        - [How to set Goals](#how-to-set-goals)
    - [CH4 MOTIVATION](#ch4-motivation)
        - [How to spot poor motivation affecting your focus](#how-to-spot-poor-motivation-affecting-your-focus)
        - [Questions to ask yourself](#questions-to-ask-yourself)
        - [How motivation works](#how-motivation-works)
        - [Possible scenarios of intrinsic / extrinsic dominance](#possible-scenarios-of-intrinsic--extrinsic-dominance)
            - [Scenario 1. Your dominant motivation is extrinisc:](#scenario-1-your-dominant-motivation-is-extrinisc)
            - [Scenario 2: Your dominant motivation is intrinsic](#scenario-2-your-dominant-motivation-is-intrinsic)
    - [CH 5 BUILD AN ENVIRONMENT THAT FACILITATES YOUR FOCUS](#ch-5-build-an-environment-that-facilitates-your-focus)
        - [Focus workspace even if you don't have an office](#focus-workspace-even-if-you-dont-have-an-office)
            - [Electronic devices](#electronic-devices)
    - [CH 6 Optimizae Your Body to Maximize Focus](#ch-6-optimizae-your-body-to-maximize-focus)
        - [Sleep](#sleep)
            - [How to improve your sleep?](#how-to-improve-your-sleep)
        - [Breaks](#breaks)
            - [Break Tips](#break-tips)
        - [Excercise](#excercise)
            - [How much exercise?](#how-much-exercise)
    - [CH 7 Your Mind: Manage Your Mind for a Laser-Sharp Focus](#ch-7-your-mind-manage-your-mind-for-a-laser-sharp-focus)

<!-- /TOC -->

## CH1 THREE COMPONENTS OF FOCUS 

1. Attention
1. Focus
1. Concentration

### 2 types of focus problems

1. Automatic Attention
1. Intentional Attention


## CH 2 Focus Danger Zones 

- your environment
- your body
- your mind

Identify what's not working when trying to focus by maintaining a *distraction log*

> Run your log for a week and look for patterns. (E/B/M) are focus  danger zones. *Roadmap is in CH 2*


## CH3 GOAL SETTING 

Set goals to achieve better focus. If you don't have goals no focus tools will give you tangible results. Goals setting helps distinguish. relevant from irrelevant. It's vital to motivation


### How to set Goals

1. Set your own meaningful goals
1. set your goals as positive statements
1. set ambitious goals
1. Make your goals SMART *Specific Measurable Attainable Realistic Timely* You can make goals specific by using the **5 W's:** *Who, What, Where, When, Why*
1. Start with the big goal and work from there. (See how to do it in Ch3 under this section).

        if on paper
        - Write big goal up top (5 W's)
        - At bottom write where you are now
        - Map major steps necessary t get where you need to be
        - Work out subgoals and milestones to make sure you capture all steps
        - Add time lines/deadlines  

> **Track your goals. Even the best goals are useless if not tracked.**

<!-- ******** Important chapter on motivation ********  -->

## CH4 MOTIVATION 

Movitvation is important to achieving life goals, but it's unreliable. 

- it's shortlived
- if you want to achieve your goals, then *you need a plan for exactly **when** and **how** you're going to execute them* (James: Clear, Achieve Your Goals)
- Motivation comes and goes

Often people become too attached to motivation and how to get motivated. It's natural that motivation fluctuates

### How to spot poor motivation affecting your focus

- manifests by lack of energy & enthusiasm
- Procrastination can indicate a wide range of underlying issues, so it may not be a motivation problem.

### Questions to ask yourself

- Why am I doing this instead of what I **should** be doing?
- How does this task I should be doing make me feel? Lazy? Flat? Bored? Uninspired? Unmotivated? Can't be bothered/whatever?

If answers align with the questions above, you have a motivation problem.
Motivation fluctuates.

> Remember that motivation fluctuates.

### How motivation works

Motiviation is intrinsic or extrinsic. 

**Intrinsic motivation:** 
is passion driven by desires of **mastery, fulfillment, or autonomy**, is more powerful and longer lasting, but it often fails us day-to-day (the grind). 

**Extrinsic motivation:**  
relies on rewards, compensation, avoiding punishment. Can work well, but wears off in time or when you run out of rewards.

For motivation to work well in the long and short term, you need the right mix, a powerful long lasting intrinsic motivator to guide you, and some short-term extrinsic strategies you can use for your day to day struggles.

### Possible scenarios of intrinsic / extrinsic dominance

#### Scenario 1. Your dominant motivation is extrinisc:  

If you struggle to focus because of low motivation, it's usually a sign of extrinisic being the main driver.

**Problem:**  

Rewards can work, but they're short-lived. You'll experience a drop in motivation.

**How to improve:**  

Rediscover your intrinsic drivers. "Can't see forest for the trees". Explore your reasons for what you're currently doing, and you may find intrinsic drivers underneath.  

Jast believes extrinsic motivators can be reframed as intrinsic ex: Financial rewards are usually about financial *independence*. See how current goals are connected to deeper desier to mastering a skill, autonomy, or some personal mission (fulfillment)

*Intrinsic Motivation Discovery Exercise:*  
on paper or electronic  

1. Ask why your doing this
1. Write down your answer and check against the ["Three Main Intrinsic Motivators"](#### 1. Your dominant motivation is extrinisc:): **mastery, fulfillment, autnonomy**
1. If your reasons can't pass the 3 filters in step 2, go back and ask yourself "Why?"  Repeat until you can clearly assign motivations.
1. Write it down and keep it handy. Record it for future use.

---

#### Scenario 2: Your dominant motivation is intrinsic

How to tell?: All boils down to mastery, autonomy, or fulfillment/purpose

**Problem:**  

It's great, but it may not be enough. In our day to day we forget our goals. Often, mundane little jobs that must be done limit our POV.

**How to improve:**

- Refresh your intrinsic motivation: sometimes all you need is a quick refresher
  - say it out loud
  - write it down and put it on the wall next to you. Whatever works for you.
- Deploy a quick extrinsic fix
  - Can the task be connected to some small reward system?
  - Can you offer yourself some material, or non-material reward?
  - Can you brag about it, or get approval praise?
  - Will you be doing this to avoid a punishment of some sort?  
  
  > Be careful not to overdo the quick fixes. Too much extrinsic motivation can kill your intrinsic. This phenomenon is called **motivatoin crowding** or the **over-justification effect** *ex: when what you used to do for fun becomes a paid job and you lose motivation*.

Movtivation fluctuates and can't be relied on. It's better to build a reliable, repeatable system.


<!-- Environment, ergonomics ch 5 

-->

## CH 5 BUILD AN ENVIRONMENT THAT FACILITATES YOUR FOCUS 

The secret to instant focus that lasts until you're ready to finish your task is the right *environment*. Stop blaming yourself and cursing your willpower (refer to Habits book).

Make sure your workspace is geared to support your ability to concentrate on the job at hand.

### Focus workspace even if you don't have an office

Most productive people recommend a quiet space as distraction free as possible where others can't disturb you (Chapter 9 has tips on dealing with an open office situation ).

Make your workspace as ergonomic as possible

#### Electronic devices

Limit the disruptions of electronic devices with all the notifications. Limit your time on the internet.

**Put a limit on your research** (big one for me).  The pursuit of information/knowledge can turn into a never ending chase after the "next shiny object". The system Jast uses is:

- Has a DB of worthy sources
- Takes notes during info gathering to monitor how much info she has and if she has new info or the same stuff
- She knows it's time to stop when she keeps getting the same information.
- Minimizes desktop clutter.

*reminder* I can use Devonthink's drop box to send things to inbox/brain dump.

[Timothy Kenny](http://timothykenny.com) says:
> "the key to focus is to isolate your roles."


## CH 6 Optimizae Your Body to Maximize Focus 

### Sleep
Get 6 hours of sleep a day. Less than that over a prolonged period is dangerous. Those with chronic sleep debt habituate and may feel like they're getting enough sleep but their abilities to concentrate does suffer. They rate their performance higher than it actually is.

#### How to improve your sleep?

- Keep a diary
- Go to bed earlier or sleep in
- In crunch times, if you have to work late, recharge during the day with power naps (15 - 20 minutes)
- Generally you want to avoid napping to a deep sleep cycle. More than 30 minutes, and you'll feel groggy.
- Follow the sleep hygiene advice from the [National Sleep Foundation](https://sleepfoundation.org/sleep-topics/sleep-hygiene)


### Breaks

Take breaks, you'll have to find your sweet spot for duration.

#### Break Tips

- Take short breaks, 5 minute breaks for every 60 - 90 minutes of work.
- Take longer breaks (20 -20 minutes) every 2 - 3 hours
- Breaks are for recharging so don't do any work related activities.

_I currently use the Pomodoro technique_

>The **Key** is not to initiate any activity that would engage you emotionally, mentally, or that is overtly physical so you don't have to calm yourself to refocus when you get back to work.

### Excercise

Excercise boosts cognitive performance. Jast highly recommends John Medina's _"Brain Rules"_.  She says it's "life changing"

Jast realized her lifestyle was not brain friendly. Despite her good sleep hygiene, breaks, and other methods, not exercising impaired her performance. Cognitive powers increase as you exercise, and decrease to pre-exercise levels when you stop.

> Exercise is the cheapest and easiest cognitive enhancer available.

#### How much exercise?

Studies show 30 minutes 2-3 times a week of aerobic can boost cognitive skills. Strength training will further enhance it. Be careful though-- too much exercise and exhaustion can impair cognitive strength.  


<!-- *** CH 7 Your Mind ***  -->
## CH 7 Your Mind: Manage Your Mind for a Laser-Sharp Focus

To focus you need your rational, cool, analytical, decision-making mind in control. If you want to focus for a decent length of time, you need to be able to control your emotions.

Why are emotions important to concentration and focus? Emotions attract our attention. We pay attention to anything that's emotional, negative, positive, joy, happiness, fear or anger. 

>Emotions can hold sway over our ability to focus

