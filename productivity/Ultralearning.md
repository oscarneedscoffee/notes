# Ultra learning

By Scott H. Young.

## Principles of Ultra learning 
1. Meta learning - first draw a map
2. Focus - sharpen your knife
3. Directness 
4. Drill - attack yourself bweakest point
5. Retrieval - test to learn
6. Feedback - don’t dodge the punches
7. Retention - don’t fill a leaky bucket
8. Intuition - dig deep before building up
9. Experimentation - explore outside your comfort zone 