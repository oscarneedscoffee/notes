## Atomic habits quotes

 - “If you want better results, then forget about setting goals. Focus on your system instead.“
- Changing our habits is challenging for two reasons: (1) we try to change the wrong thing and (2) we try to change our habits in the wrong way.
 - Behavior that is incongruent with the self will not last.
 - The ultimate form of intrinsic motivation is when a habit becomes part of your identity. It’s one thing to say I’m the type of person who wants this. It’s something very different to say I’m the type of person who is this.
 - Many people walk through life in a cognitive slumber, blindly following the norms attached to their identity. “I’m terrible with directions.” “I’m not a morning person.”
 - The concept of identity-based habits is our first introduction to another key theme in this book: feedback loops.
 - Ultimately, your habits matter because they help you become the type of person you wish to be. They are the channel through which you develop your deepest beliefs about yourself. Quite literally, you become your habits.
 - The most effective way to change your habits is to focus not on what you want to achieve, but on who you wish to become.
 - Habits do not restrict freedom. They create it. In fact, the people who don’t have their habits handled are often the ones with the least amount of **freedom**.
 - The process of building a habit can be divided into four simple steps: cue, craving, response, and reward.
 - The ultimate purpose of habits is to solve the problems of life with as little energy and effort as possible.
 - Many people think they lack motivation when what they really lack is clarity. It is not always obvious when and where to take action. Some people spend their entire lives waiting for the time to be right to make an improvement. (Ch 5)

## scratch 

- Parts of habit changes begins with identity then process and finally outcomes. 
- 4 laws of behavior change and the inverse to break bad habits (ch 3,53-54)

Two step process to identity change
1. Decide the type of person you want to be. 
2. Prove it to yourself with small wins.

- implementation intention (ch 5). the format for creating an implementation intention is: “When situation X arises, I will perform response Y.”