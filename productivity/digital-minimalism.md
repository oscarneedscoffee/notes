# Digital Minimalism 
by Cal Newport

## 3 Principles of Digital Minimalism

1. Clutter is costly
2. Optimization is important
3. Intentionality is satisfying

Newport advocates for a __technological philosophy of use__-- this is a complete, from the ground up review of what a piece of technology adds to our lives _(part 1 of book)_

<!------------------ The New Economy --------------------------------->
## An Argument for Principle 1: Thoreau's New Economy

Thoreau's pragmatic side is often overlooked in Walden. He compares the cost of his cabin, and the one day of work required to sustain it, versus the much larger overhead of a large farm. Sure, the farm work gives you better furnishings, but is is worth it?

> The cost of a thing is the amount of what I will call life which is required to be exchanged for it, immediately or in the long run  
>\- Thoreau  _(see farmer's example p. 39)_

What these farmers are actually getting from all this life they sacrifice is slightly newer stuff according to Newport

Thoreau's new economies demand you balance profit against the costs measured in terms of your life.  The costs of digital distractions compound. This can leave you crushed and smothered under the demands of your time and attention like Thoreau's farmers.

>All you get from your sacrifice are slightly nicer trinkets
*This is why clutter is so dangerous*

So what made Thoreau so insightful? It was nothing that hadn't already been said-- it was his obsession with calculations. That helped us move from vague subjectiveness that there are tradeoffs in clutter and forces us to confront it more precisely.  

>Treat the minutes of your lives as a concrete and valuable substance.

<!--------------- The Return Curve -------------------->
## An Argument for Principle 2: The Return Curve  
